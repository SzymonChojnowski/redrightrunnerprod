﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectPool : MonoBehaviour, IMainGameEvents
{
    PooledObject prefab;
    List<PooledObject> availableObjects = new List<PooledObject>();

    void IMainGameEvents.OnGameLost()
    {
        ReturnAllObjectsToPool();
    }

    public void ReturnAllObjectsToPool()
    {
        foreach (Transform child in transform)
        {
            if (child.gameObject.activeInHierarchy)
            {
                child.GetComponent<PooledObject>().ReturnToPool();
            }
        }
    }
    public static ObjectPool GetPool(PooledObject prefab)
    {
        GameObject obj;
        ObjectPool pool;
        if (Application.isEditor)
        {
            obj = GameObject.Find(prefab.name + " Pool");
            if (obj)
            {
                pool = obj.GetComponent<ObjectPool>();
                if (pool)
                {
                    return pool;
                }
            }
        }
        
        obj = new GameObject(prefab.name + " Pool");
        EventListener.Instance.AddListener(obj);
        pool = obj.AddComponent<ObjectPool>();
        pool.prefab = prefab;
        return pool;
    }

    public PooledObject GetObject()
    {
        PooledObject obj;
        int lastAvailableIndex = availableObjects.Count - 1;
       
        if (lastAvailableIndex >= 0)
        {
            obj = availableObjects[lastAvailableIndex];
            availableObjects.RemoveAt(lastAvailableIndex);
            obj.gameObject.SetActive(true);
        }
        else
        {
            obj = Instantiate(prefab);
            obj.transform.SetParent(transform, false);
            obj.Pool = this;
        }
        return obj;
    }
    public void AddObject(PooledObject obj)
    {
        availableObjects.Add(obj);
        obj.gameObject.SetActive(false);
    }

}