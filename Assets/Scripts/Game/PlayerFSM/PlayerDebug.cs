﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerDebug : BasePlayerState {


    public override void HandleInput(PlayerControl player)
    {
        if (player.IsGrounded && (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)))
        { 
            
        }
    }

    public override void OnUpdate(PlayerControl player)
    {
    }

    public override void CollisionEnter(PlayerControl player, Collision2D coll)
    {
        base.CollisionEnter(player, coll);
    }

    public override void TriggerEnter(PlayerControl player, Collider2D collision)
    {
        base.TriggerEnter(player, collision);
    }
}
