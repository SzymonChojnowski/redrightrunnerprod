﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerRun : BasePlayerState
{
    public override void HandleInput(PlayerControl player)
    {
        if (player.IsGrounded && (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0)))
        {
            foreach (Touch touch in Input.touches)
            {
                int id = touch.fingerId;
                if (EventSystem.current.IsPointerOverGameObject(id))
                {

                }
                else
                {
                    player.PlayerRB.velocity = Vector2.up * player.JumpDistance;
                    ToState(player, STATE_JUMPING);
                }
            }
#if UNITY_EDITOR
            player.PlayerRB.velocity = Vector2.up * player.JumpDistance;
            ToState(player, STATE_JUMPING);
#endif
        }

    }

    public override void CollisionEnter(PlayerControl player, Collision2D coll)
    {
        base.CollisionEnter(player, coll);
    }

    public override void TriggerEnter(PlayerControl player, Collider2D collision)
    {
        base.TriggerEnter(player, collision);
    }

    public override void OnUpdate(PlayerControl player)
    {
        player._playedTime += Time.deltaTime;
    }

}
