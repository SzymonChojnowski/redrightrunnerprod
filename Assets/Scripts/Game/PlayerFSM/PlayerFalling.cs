﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerFalling : BasePlayerState
{

    public override void OnEnter(PlayerControl player)
    {
        base.OnEnter(player);
        player.Animator.SetBool("isJumping", true);
    }

    public override void OnExit(PlayerControl player)
    {
        base.OnExit(player);
        player.Animator.SetBool("isJumping", false);
        player.doubleKillChance = false;
    }

    public override void OnUpdate(PlayerControl player)
    {
        player._playedTime += Time.deltaTime;
        if(player.IsGrounded)
        {
            ToState(player, STATE_RUNNING);
        }
    }

    public override void CollisionEnter(PlayerControl player, Collision2D coll)
    {
        if (Util.Instance.IsInLayerMask(coll.gameObject.layer, Util.Instance.ENEMY))
        {
            if (coll.collider.usedByEffector)
            {
                GameData.Instance.monstersKills += 1;
                Hit(player);
                if (player.doubleKillChance)
                {
                    GameController.Instance.TextInfoUI.SetUIText("DOUBLE KILL ! !", "You rock [+5 coins]");
                    GameController.Instance.coinCounter.Amount += 5;
                }
                player.doubleKillChance = true;
            }
            else
            {
                GameData.Instance.monstersKills += 1;
                if (!player.isInvulnerability)
                {
                    for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
                    {
                        ExecuteEvents.Execute<IPlayerEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnPlayerHurt());
                    }
                }
                else
                {
                    if (player.doubleKillChance)
                    {
                        GameController.Instance.TextInfoUI.SetUIText("DOUBLE KILL ! !", "You rock");
                        player.doubleKillChance = false;
                    }
                }
            }
        }
    }

    public override void TriggerEnter(PlayerControl player, Collider2D collision)
    {
        base.TriggerEnter(player, collision);
    }

    public override void HandleInput(PlayerControl player) { }
    
}
