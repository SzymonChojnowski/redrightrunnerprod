﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PlayerControl : PooledObject, IPlayerEvents
{
    #region Fields
    [Header("Player")]
    [SerializeField]
    private float _moveSpeed = 5f;
    [SerializeField]
    private float _jumpDistance = 6.3f;
    [SerializeField]
    private float _jumpTime = 0.32f;
    [Space]
    [SerializeField]
    private AudioClip _soundEffectJump;
    [SerializeField]
    private AudioClip _soundEffectHit;

    private IPlayerStates state;


    #endregion

    #region Properties

    public Rigidbody2D PlayerRB { get; set; }
    public PolygonCollider2D Coll { get; set; }

    public Animator Animator { get; set; }
    public bool IsGrounded { get; set; }
    public bool HasStarted { get; set; }
    public bool IsJumping { get; set; }
    public static bool IsMagnet { get; set; }
    public AudioSource AudioSource { get; set; }
    public MapRemover MapRemover { get; set; }


    public bool doubleKillChance;

    public float MoveSpeed
    {
        get
        {
            return _moveSpeed;
        }
        set
        {
            _moveSpeed = value;
        }
    }

    public int CoinMultiplayer
    {
        get
        {
            return _coinMultiplayer;
        }

        set
        {
            _coinMultiplayer = value;
        }
    }

    public float JumpDistance
    {
        get
        {
            return _jumpDistance;
        }

        set
        {
            _jumpDistance = value;
        }
    }

    public float JumpTime
    {
        get
        {
            return _jumpTime;
        }

        set
        {
            _jumpTime = value;
        }
    }

    public AudioClip SoundEffectJump
    {
        get
        {
            return _soundEffectJump;
        }

        set
        {
            _soundEffectJump = value;
        }
    }

    public AudioClip SoundEffectHit
    {
        get
        {
            return _soundEffectHit;
        }

        set
        {
            _soundEffectHit = value;
        }
    }

    public IPlayerStates State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
        }
    }

    #endregion

    public float _orginalSpeed, _orginalJumpTimeCounter,
    _jumpTimeCounter, _playedTime;
    public int _coinMultiplayer = 1;
    public bool isInvulnerability;

    void IPlayerEvents.OnPlayerHurt()
    {
        isInvulnerability = true;
        MoveSpeed = _orginalSpeed;
        GameData.Instance.playedTime = Mathf.RoundToInt(_playedTime);
        _playedTime = 0;
        MoveSpeed = _orginalSpeed;
        CoinMultiplayer = 1;
        _jumpTimeCounter = _orginalJumpTimeCounter;
        IsMagnet = false;
        State.ToState(this, BasePlayerState.STATE_START);
    }

    void Awake()
    {
        EventListener.Instance.AddListener(gameObject);
        PlayerRB = GetComponent<Rigidbody2D>();
        Coll = GetComponent<PolygonCollider2D>();
        Animator = GetComponent<Animator>();
        MapRemover = GetComponentInChildren<MapRemover>();
        AudioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        State = BasePlayerState.STATE_START;
        _orginalSpeed = MoveSpeed;
        _orginalJumpTimeCounter = _jumpTimeCounter;
        _playedTime = 0;
        IsMagnet = false;
        isInvulnerability = true;
        StartCoroutine("StartRun");
        Animator.keepAnimatorControllerStateOnDisable = true;
    }

    void OnEnable()
    {
        StartCoroutine("StartRun");
    }

    IEnumerator StartRun()
    {
        yield return new WaitUntil(() => GameController.Instance.State == BaseState.STATE_PLAYING);
        State = BasePlayerState.STATE_RUNNING;
        isInvulnerability = false;
    }

    private void FixedUpdate()
    {
        if (State != BasePlayerState.STATE_START)
        {
            PlayerRB.velocity = new Vector2(_moveSpeed, PlayerRB.velocity.y);
        }
    }

    void Update()
    {
        IsGrounded = Physics2D.IsTouchingLayers(Coll, Util.Instance.GROUND);
        State.OnUpdate(this);
        MapRemover.transform.position = transform.position - new Vector3(50, 0, 0);
        HandleInput();
    }

    private void HandleInput()
    {
        State.HandleInput(this);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        State.CollisionEnter(this, coll);

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        State.TriggerEnter(this, collision);
    }


    public void PlaySound(AudioClip audioClip)
    {
        AudioSource.PlayOneShot(audioClip);
    }

    void IPlayerEvents.OnPlayerReachedGeneration(PlayerControl player)
    {
        // throw new System.NotImplementedException();
    }
}
