﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class BasePlayerState : IPlayerStates
{
    static public readonly IPlayerStates STATE_JUMPING = new PlayerJump();
    static public readonly IPlayerStates STATE_RUNNING = new PlayerRun();
    static public readonly IPlayerStates STATE_FALLING = new PlayerFalling();
    static public readonly IPlayerStates STATE_START = new PlayerStart();

    public virtual void OnEnter(PlayerControl player)
    {

    }

    public virtual void OnExit(PlayerControl player)
    {

    }

    public virtual void ToState(PlayerControl player, IPlayerStates targetState)
    {
        player.State.OnExit(player);
        player.State = targetState;
        player.State.OnEnter(player);
    }

    public virtual void CollisionEnter(PlayerControl player, Collision2D coll)
    {
        if (Util.Instance.IsInLayerMask(coll.gameObject.layer, Util.Instance.ENEMY))
        {
                if (coll.collider.usedByEffector)
            {
                GameData.Instance.monstersKills += 1;
                Hit(player);
                ToState(player, STATE_FALLING);

            }
            else
            {
                GameData.Instance.monstersKills += 1;
                if (!player.isInvulnerability)
                {
                    for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
                    {
                        ExecuteEvents.Execute<IPlayerEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnPlayerHurt());
                    }
                }
                else
                {
                    ToState(player, STATE_FALLING);
                }
            }
        }

    }

    public virtual void TriggerEnter(PlayerControl player, Collider2D collision)
    {
        if (collision.CompareTag("Map_Generation_Starter"))
        {
            player.MoveSpeed += 0.15f;

            for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
            {
                ExecuteEvents.Execute<IPlayerEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnPlayerReachedGeneration(player));
            }
        }
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.ENEMY))
        {
            if (!player.isInvulnerability)
            {
                for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
                {
                    ExecuteEvents.Execute<IPlayerEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnPlayerHurt());
                }
            }
            Hit(player);
            GameData.Instance.monstersKills += 1;
            ToState(player, STATE_FALLING);
        }
    }

    public abstract void OnUpdate(PlayerControl player);
    public abstract void HandleInput(PlayerControl player);

    protected void Hit(PlayerControl player)
    {
        player.PlayerRB.AddForce(Vector2.up * 800);
        player.PlaySound(player.SoundEffectHit);
    }
}
