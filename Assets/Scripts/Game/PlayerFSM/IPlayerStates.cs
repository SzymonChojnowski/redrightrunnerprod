﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerStates {

    void OnUpdate(PlayerControl player);

    void HandleInput(PlayerControl player);

    void OnEnter(PlayerControl player);

    void OnExit(PlayerControl player);

    void ToState(PlayerControl player, IPlayerStates targetState);

    void CollisionEnter(PlayerControl player, Collision2D coll);

    void TriggerEnter(PlayerControl player, Collider2D collision);

}
