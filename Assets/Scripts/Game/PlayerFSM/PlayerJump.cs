﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : BasePlayerState
{

    public override void OnEnter(PlayerControl player)
    {
        base.OnEnter(player);
        player.Animator.SetBool("isJumping", true);
        player._jumpTimeCounter = player.JumpTime;
        player.PlaySound(player.SoundEffectJump);
    }

    public override void OnExit(PlayerControl player)
    {
        base.OnExit(player);
        player.Animator.SetBool("isJumping", false);
    }

    public override void OnUpdate(PlayerControl player)
    {
        player._playedTime += Time.deltaTime;

        if (player._jumpTimeCounter > 0)
        {
            if ((Input.GetButton("Jump") || Input.GetMouseButton(0)))
            {
                player.PlayerRB.velocity = Vector2.up * player.JumpDistance;
            }
            player._jumpTimeCounter -= Time.deltaTime;
        }
        else
        {
            ToState(player, STATE_FALLING);
        }
    }

    public override void CollisionEnter(PlayerControl player, Collision2D coll)
    {
        base.CollisionEnter(player, coll);
    }

    public override void TriggerEnter(PlayerControl player, Collider2D collision)
    {
        base.TriggerEnter(player, collision);
    }

    public override void HandleInput(PlayerControl player) 
    {
    }

}
