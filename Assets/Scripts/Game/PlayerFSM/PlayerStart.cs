﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStart : BasePlayerState
{
    public override void HandleInput(PlayerControl player)
    {

    }

    public override void OnEnter(PlayerControl player)
    {
        player.Animator.SetBool("isJumping", false);
        base.OnEnter(player);
    }

    public override void OnExit(PlayerControl player)
    {
        base.OnExit(player);
    }

    public override void OnUpdate(PlayerControl player)
    {

    }
    public override void CollisionEnter(PlayerControl player, Collision2D coll)
    {
    }

    public override void TriggerEnter(PlayerControl player, Collider2D collision)
    {
    }


}
