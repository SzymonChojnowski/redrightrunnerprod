﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : Enemy
{
    private GameObject player;
    private Animator anim;
    [SerializeField]
    private Fireball fireball;

    private bool firstSpawn = true;


    void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(SpawnFireball());
        firstSpawn = false;
    }

    private void OnEnable()
    {
        if(!firstSpawn)
        {
            StartCoroutine(SpawnFireball());
        }
    }

    private IEnumerator SpawnFireball()
    {
        yield return new WaitForSeconds(1f);
        anim.SetBool("WantShoot", true);
        yield return new WaitForSeconds(2f);
        //Fireball spawn = fireball.GetPooledInstance<Fireball>();
       // spawn.transform.position = transform.position + new Vector3(0, 1);
       // spawn.transform.rotation = Quaternion.identity;
       // spawn.gameObject.SetActive(true);

    }

    new private void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (collision.gameObject.CompareTag("Fireball"))
        {
            anim.SetBool("WantShoot", false);
            StartCoroutine(SpawnFireball());
        }
    }
}
