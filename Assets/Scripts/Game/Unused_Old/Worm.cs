﻿using UnityEngine;
public class Worm : Enemy
{
    [SerializeField]
    private GameObject _leftEdge, _rightEdge;

    [Header("Move")]
    [SerializeField]
    private float moveSpeed = 0.05f;
    [SerializeField]
    private float stopTime = 1;

    [Header("Chase")]
    [SerializeField]
    [Range(0.0f, 1f)]
    private float chaserChance = 0.5f;
    [SerializeField]
    private float chaserDistance = 3;
    [SerializeField]
    private float chaseSpeed = 0.10f;

    private Rigidbody2D body;
    private Animator anim;
    private SpriteRenderer render;
    private float move, timer;
    private bool isStopped, isRight, isChaser;
    private readonly int maskLayer = 8;

    public GameObject LeftEdge
    {
        get
        {
            return _leftEdge;
        }

        set
        {
            _leftEdge = value;
        }
    }

    public GameObject RightEdge
    {
        get
        {
            return _rightEdge;
        }

        set
        {
            _rightEdge = value;
        }
    }

    void Start()
    {
        body = GetComponentInParent<Rigidbody2D>();
        render = GetComponentInParent<SpriteRenderer>();
        if (Random.value < chaserChance)
        {
            isChaser = true;
        }
    }

    void Update()
    {
        float distance = Vector2.Distance(transform.position, GameObject.Find("Player").transform.position);
        Vector2 direction = transform.position - GameObject.Find("Player").transform.position;
        body.MovePosition(body.position + Vector2.left * move);

        if (isChaser && (distance < chaserDistance))
        {
            if (direction.x > 0)
            {
                isRight = false;
                move = chaseSpeed;
            }
            else
            {
                isRight = true;
                move = -chaseSpeed;
            }
            // anim.SetBool("isChasing", true);
        }
        else
        {
            //anim.SetBool("isChasing", false);
            if (isStopped)
            {
                timer += Time.deltaTime;
                if (timer >= stopTime)
                {
                    isStopped = false;
                    timer = 0;
                }
            }
            else
            {
                if (isRight == false)
                {
                    move = moveSpeed;
                    render.flipX = false;
                }
                else
                {
                    move = -moveSpeed;
                    render.flipX = true;
                }
            }
        }
        RaycastHit2D hitLeft = Physics2D.Raycast(LeftEdge.transform.position, Vector2.down, 0.5f, 1 << maskLayer);
        RaycastHit2D hitRight = Physics2D.Raycast(RightEdge.transform.position, Vector2.down, 0.5f, 1 << maskLayer);
        if (isRight == false)
        {
            if (hitLeft.collider == null)
            {
                isStopped = true;
                move = 0;
                render.flipX = true;
            }
        }
        if (isRight)
        {
            if (hitRight.collider == null)
            {

                isStopped = true;
                move = 0;
                render.flipX = false;
            }
        }
    }    
}
