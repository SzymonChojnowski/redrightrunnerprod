﻿using UnityEngine;

public class Fireball : PooledObject
{
    [SerializeField]
    private float minSpeed = 300f;
    [SerializeField]
    private float maxSpeed = 500f;
    private Rigidbody2D body;
    private bool canFly;

    private void Awake()
    {
        body = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        canFly = true; 
        Fly();
    }

    private void Fly()
    {
        float speed = Random.Range(minSpeed, maxSpeed);
        body.AddForceAtPosition(new Vector2(0, speed), new Vector2(gameObject.transform.position.x, gameObject.transform.position.y));
    }

    private void OnEnable()
    {
        if (canFly)
        {
            Fly();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            ReturnToPool();
        }
        else if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            ReturnToPool();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }
}
