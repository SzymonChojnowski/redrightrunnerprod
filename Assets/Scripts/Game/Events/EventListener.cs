﻿using System.Collections.Generic;
using UnityEngine;

public class EventListener : Singleton<EventListener>
{
    public List<GameObject> listeners;

    void Start()
    {
        if (listeners == null)
        {
            listeners = new List<GameObject>();
        }
        GameObject[] obj = GameObject.FindGameObjectsWithTag("Listener");
        listeners.AddRange(obj);
    }

    public void AddListener(GameObject go)
    {
        if (!listeners.Contains(go))
        {
            listeners.Add(go);
        }
    }

    public void DeleteListener(GameObject go)
    {
        if (listeners.Contains(go))
        {
            listeners.Remove(go);
        }
    }
}
