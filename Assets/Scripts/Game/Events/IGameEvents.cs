﻿using UnityEngine.EventSystems;
using UnityEngine;

public interface IMainGameEvents : IEventSystemHandler
{
    void OnGameLost();
}