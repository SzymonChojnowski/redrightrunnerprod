﻿using UnityEngine.EventSystems;
using UnityEngine;

public interface IPowerUpEvents : IEventSystemHandler
{
    void OnPowerUpCollected(PowerUp powerUp, PlayerControl player);

    void OnPowerUpExpired(PowerUp powerUp, PlayerControl player);
}