﻿using UnityEngine.EventSystems;
using UnityEngine;

public interface IPlayerEvents : IEventSystemHandler
{
    void OnPlayerHurt();
    void OnPlayerReachedGeneration(PlayerControl player);
}

