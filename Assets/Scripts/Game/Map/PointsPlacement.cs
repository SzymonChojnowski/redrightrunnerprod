﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsPlacement : MonoBehaviour
{
    [Range(0.0f, 1.0f)]
    [SerializeField]
    private float _pointsChance = 0.3f;
    [SerializeField]
    private int _pooledPoints = 3;
    [SerializeField]
    private List<Points> _points = new List<Points>();

    #region Properties

    public float PointsChance
    {
        get
        {
            return _pointsChance;
        }

        set
        {
            _pointsChance = value;
        }
    }

    public int PooledPoints
    {
        get
        {
            return _pooledPoints;
        }

        set
        {
            _pooledPoints = value;
        }
    }

    public List<Points> Points
    {
        get
        {
            return _points;
        }

        set
        {
            _points = value;
        }
    }

    #endregion

    public void PlacePreObjects()
    {
        for (int i = 0; i < PooledPoints; i++)
        {
            for (int j = 0; j < Points.Count; j++)
            {
                Points prefab = Points[j];
                prefab.GetPooledInstance<Points>();
                prefab.gameObject.SetActive(true);
            }
        }
    }

    public void PlaceObject(Vector2 cords)
    {
        if (Random.value < PointsChance)
        {
            Points prefab = Points[Random.Range(0, Points.Count)];
            Points spawn = prefab.GetPooledInstance<Points>();
            spawn.transform.position = new Vector3(cords.x, cords.y, 0);
            spawn.transform.rotation = Quaternion.identity;
            spawn.gameObject.SetActive(true);

            for (int point = 0; point < spawn.transform.childCount; point++)
            {
                spawn.transform.GetChild(point).gameObject.transform.localPosition = prefab.transform.GetChild(point).gameObject.transform.localPosition;
                spawn.transform.GetChild(point).gameObject.SetActive(true);
            }
        }
    }
}
