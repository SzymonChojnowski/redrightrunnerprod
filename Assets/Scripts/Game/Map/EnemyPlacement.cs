﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyPlacement : MonoBehaviour
{
    [SerializeField]
    private int _pooledEnemies = 2;
    [SerializeField]
    private List<Enemy> _enemies = new List<Enemy>();
    [SerializeField]
    [Range(0.0f, 1f)]
    private float _enemyChance = 0.5f;

    #region Properties

    public int PooledEnemies
    {
        get
        {
            return _pooledEnemies;
        }

        set
        {
            _pooledEnemies = value;
        }
    }

    public List<Enemy> Enemies
    {
        get
        {
            return _enemies;
        }

        set
        {
            _enemies = value;
        }
    }

    public float EnemyChance
    {
        get
        {
            return _enemyChance;
        }

        set
        {
            _enemyChance = value;
        }
    }

    #endregion

    public void PlacePreObjects()
    {
        for (int i = 0; i < PooledEnemies; i++)
        {
            for (int j = 0; j < Enemies.Count; j++)
            {
                Enemy prefab = Enemies[j];
                prefab.GetPooledInstance<Enemy>();
                prefab.gameObject.SetActive(true);
            }
        }
    }

    public bool PlaceEnemy(Vector2 cords)
    {
        if (Random.value < EnemyChance)
        {
            Enemy prefab = Enemies[Random.Range(0, Enemies.Count)];
            Enemy spawn = prefab.GetPooledInstance<Enemy>();
            spawn.transform.position = new Vector3(cords.x, cords.y, 0);
            spawn.transform.rotation = Quaternion.identity;
            spawn.transform.localPosition = new Vector3(cords.x, cords.y, 0);
            spawn.gameObject.SetActive(true);
            return true;
        }
        return false;
    }
}


