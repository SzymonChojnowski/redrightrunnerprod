﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGeneration : MonoBehaviour
{
    public enum MapElements
    {
        Bridge,
        Bridge_Left,
        Bridge_Right,
        Garbage_Collector,
        Ground_Mid,
        Ground_Mid_Left,
        Ground_Mid_Right,
        Ground_Top,
        Ground_Top_Left,
        Ground_Top_Right,
        Map_Generation_Starter,
        Spikes,
        Water,
        Water_Top
    }

    private enum TileType
    {
        Left,
        Right,
        Center,
        Water
    };

    [System.Serializable]
    public class MapElementsDetails
    {
        public MapElements id;
        public MapElement mapElement;
        public int preLoadCount;
    }

    #region Fields

    [Header("Platforms")]
    [SerializeField]
    private PlatformProperties _platformsProperties;
    [SerializeField]
    [Header("Holes")]
    private HoleProperties _holesProperties;

    [Header("Start")]
    [SerializeField]
    private Vector2 _startPlatformCords;
    [SerializeField]
    private int _startSize = 10;

    [Header("Other")]
    [SerializeField]
    private int _mapGenerationStarter = 15;
    [SerializeField]
    private int _garbageCollectorHeight = -150;
    [SerializeField]
    private PlayerControl _player;

    #endregion

    #region Private Variables

    private int _blockNum = 1;
    private int _blockHeight = 2;
    private bool _isHole;
    private Vector2 _tileSpawnOffset = new Vector2(1, 1);

    #endregion

    #region Properties

    public ObjectPlacement ObjectPlacement1 { get; set; }
    public InteractiveObjectPlacement InteractiveObjectPlacement { get; set; }
    public EnemyPlacement EnemyPlacement1 { get; set; }
    public PointsPlacement PointsPlacement { get; set; }
    public PowerUpPlacement PowerUpPlacement1 { get; set; }

    public Vector2 StartPlatformCords
    {
        get
        {
            return _startPlatformCords;
        }

        set
        {
            _startPlatformCords = value;
        }
    }

    public int MapGenerationStarter
    {
        get
        {
            return _mapGenerationStarter;
        }

        set
        {
            _mapGenerationStarter = value;
        }
    }

    public int GarbageCollectorHeight
    {
        get
        {
            return _garbageCollectorHeight;
        }

        set
        {
            _garbageCollectorHeight = value;
        }
    }

    public PlayerControl Player
    {
        get
        {
            return _player;
        }

        set
        {
            Player = value;
        }
    }

    public Vector2 TileSpawnOffset
    {
        get
        {
            return _tileSpawnOffset;
        }

        set
        {
            _tileSpawnOffset = value;
        }
    }

    public PlatformProperties PlatformsProperties
    {
        get
        {
            return _platformsProperties;
        }

        set
        {
            _platformsProperties = value;
        }
    }

    public HoleProperties HolesProperties
    {
        get
        {
            return _holesProperties;
        }

        set
        {
            _holesProperties = value;
        }
    }

    public int StartSize
    {
        get
        {
            return _startSize;
        }

        set
        {
            _startSize = value;
        }
    }


    #endregion

    #region Monobehaviour

    void Start()
    {
        SetReferences();
        GeneratePrefabs();
        //PlacePreObjects();
        GenerateStartPoint();
        GenerateMap();
        CreatePlayer();

    }

    #endregion

    #region Private Methods

    private void SetReferences()
    {
        ObjectPlacement1 = GetComponent<ObjectPlacement>();
        InteractiveObjectPlacement = GetComponent<InteractiveObjectPlacement>();
        EnemyPlacement1 = GetComponent<EnemyPlacement>();
        PointsPlacement = GetComponent<PointsPlacement>();
        PowerUpPlacement1 = GetComponent<PowerUpPlacement>();
    }

    private void PlacePreObjects()
    {
        ObjectPlacement1.PlacePreObjects();
        InteractiveObjectPlacement.PlacePreObjects();
        PointsPlacement.PlacePreObjects();
        PowerUpPlacement1.PlacePreObjects();
        EnemyPlacement1.PlacePreObjects();
    }

    private void GenerateStartPoint()
    {
        for (int i = 0; i < StartSize; i++)
        {
            SpawnElement(MapElements.Ground_Top, new Vector2(-StartSize + i, StartPlatformCords.y));
        }
    }

    private void CreatePlayer()
    {
        PlayerControl prefab = Player;
        PlayerControl spawn = prefab.GetPooledInstance<PlayerControl>();
        spawn.transform.position = new Vector3(-StartSize + 1, StartPlatformCords.y + 1, 0);
        spawn.transform.rotation = Quaternion.identity;
        spawn.gameObject.SetActive(true);

    }
    private void ChoosePlatform(int platform)
    {
        if (!SetupHole())
        {
            int platformSize = Mathf.RoundToInt(Random.Range(PlatformsProperties.MinSize, PlatformsProperties.MaxSize));
            int coinPlacement = Mathf.RoundToInt(platformSize / 2);
            if (Random.value < PlatformsProperties.BridgeChance)
            {
                CreateBridge(platformSize, coinPlacement);
            }
            else if (Random.value < PlatformsProperties.ColumnsChance)
            {
                CreateColumns();
            }
            else
            {
                CreatePlatform(platform, platformSize, coinPlacement);
            }
        }
    }

    private void GenerateGround(int offset, TileType tileType)
    {
        for (int gMid = 1; gMid < PlatformsProperties.GroundHeight; gMid++)
        {
            switch (tileType)
            {
                case TileType.Left:
                    SpawnElement(MapElements.Ground_Mid_Left, new Vector2(_blockNum, (_blockHeight - gMid) - offset));
                    break;
                case TileType.Right:
                    SpawnElement(MapElements.Ground_Mid_Right, new Vector2(_blockNum, (_blockHeight - gMid) - offset));
                    break;
                case TileType.Water:
                    if (gMid == 1)
                    {
                        SpawnElement(MapElements.Water_Top, new Vector2(_blockNum, (_blockHeight - gMid) - offset));
                    }
                    else
                    {
                        SpawnElement(MapElements.Water, new Vector2(_blockNum, (_blockHeight - gMid) - offset));
                    }
                    break;
                default:
                    SpawnElement(MapElements.Ground_Mid, new Vector2(_blockNum, (_blockHeight - gMid) - offset));
                    break;
            }
        }
    }

    private bool SetupHole()
    {
        if (_isHole)
        {
            _isHole = false;
            return false;
        }
        else
        {
            bool shouldSpawnHole = (Random.value < HolesProperties.HoleChance) ? true : false;
            if (!shouldSpawnHole)
            {
                return false;
            }
            else
            {
                _isHole = true;
                GenerateHole();
                return true;
            }
        }
    }

    private void GenerateHole()
    {
        int hazardSize = Mathf.RoundToInt(Random.Range(HolesProperties.MinSize, HolesProperties.MaxSize));
        if (Random.value < HolesProperties.SpikeChance)
        {
            for (int tiles = 0; tiles < hazardSize; tiles++)
            {
                SpawnElement(MapElements.Spikes, new Vector2(_blockNum, _blockHeight - HolesProperties.SpikesOffset));
                GenerateGround(HolesProperties.SpikesOffset, TileType.Center);
                _blockNum++;
            }
        }
        else
        {
            _blockNum += hazardSize;
        }
    }

    private void CreateBridge(int platformSize, int coinPlacement)
    {
        bool isEnemyOnPlatform = false;
        _blockHeight = _blockHeight + Random.Range(PlatformsProperties.MaxDrop, PlatformsProperties.MaxHeight);

        for (int tiles = 1; tiles <= platformSize; tiles++)
        {
            if (tiles == 1)
            {
                SpawnTileWithGround(MapElements.Bridge_Left, TileType.Left);
            }
            else if (tiles == platformSize)
            {
                SpawnTileWithGround(MapElements.Bridge_Right, TileType.Right);
                PowerUpPlacement();
            }
            else
            {
                if (tiles == coinPlacement)
                {
                    PointPlacement();
                }
                if (!isEnemyOnPlatform)
                {
                    isEnemyOnPlatform = EnemyPlacement();
                }
                SpawnTileWithGround(MapElements.Bridge, TileType.Water);
            }
            _blockNum++;
        }
    }

    //private MapElement GenerateMidGround(int platformSize, Vector3 position)
    //{
        
        //SpawnElement(MapElements.Ground_Mid, new Vector2(_blockNum, (_blockHeight - gMid) - offset));
        //MapElement prefab = mp.mapElement;
        //MapElement spawn = prefab.GetPooledInstance<MapElement>();
        //spawn.transform.position = position;
        //spawn.transform.rotation = Quaternion.identity;
        
        //spawn.gameObject.SetActive(true);
        //return spawn;
    //}

    private MapElement xd(MapElements obj, Vector3 position)
    {
        MapElementsDetails mp = PlatformsProperties.MapElements.Find(x => x.id == obj);
        MapElement prefab = mp.mapElement;
        MapElement spawn = prefab.GetPooledInstance<MapElement>();
        spawn.transform.position = position;
        spawn.transform.rotation = Quaternion.identity;
        spawn.gameObject.SetActive(true);
        return spawn;
    }

    private void CreatePlatform(int platform, int platformSize, int coinPlacement)
    {
        bool isEnemyOnPlatform = false;
        if (platform == 1)
        {
            isEnemyOnPlatform = true;
        }
        _blockHeight = _blockHeight + Random.Range(PlatformsProperties.MaxDrop, PlatformsProperties.MaxHeight);
        for (int tiles = 1; tiles <= platformSize; tiles++)
        {
            if (tiles == 1)
            {
                SpawnTileWithGround(MapElements.Ground_Top_Left, TileType.Left);
                ObjectPlacement(tiles, platformSize, isEnemyOnPlatform);
            }
            else if (tiles == platformSize)
            {
                SpawnTileWithGround(MapElements.Ground_Top_Right, TileType.Right);
                ObjectPlacement(tiles, platformSize, isEnemyOnPlatform);
                PowerUpPlacement();
            }
            else
            {
                if (!isEnemyOnPlatform)
                {
                    isEnemyOnPlatform = EnemyPlacement();
                }
                if (tiles == coinPlacement)
                {
                    PointPlacement();
                }
                SpawnTileWithGround(MapElements.Ground_Top, TileType.Center);
                ObjectPlacement(tiles, platformSize, isEnemyOnPlatform);
            }
            _blockNum++;
        }
    }

    private void CreateColumns()
    {
        int platformSize = 12;
        int offset = 1;
        MapElements btm = randTile(true);
        for (int tiles = 1; tiles <= platformSize; tiles++)
        {
            if (tiles == 1)
            {
                SpawnTileWithGround(MapElements.Ground_Top_Left, TileType.Left);
            }
            else if (tiles == 2)
            {
                SpawnTileWithGround(MapElements.Ground_Top, TileType.Center);
                PowerUpPlacement();
            }
            else if (tiles == platformSize)
            {
                SpawnTileWithGround(MapElements.Ground_Top_Right, TileType.Right);
                PointPlacement();
            }
            else
            {
                if (tiles % 3 == 0)
                {
                    SpawnElement(MapElements.Bridge, new Vector2(_blockNum, _blockHeight + offset));
                    offset++;
                }
                else
                {
                    GenerateGround(0, TileType.Center);
                }
                SpawnElement(btm, new Vector2(_blockNum, _blockHeight));
            }
            GenerateGround(0, TileType.Center);
            _blockNum++;
        }
    }

    private MapElements randTile(bool ground)
    {
        if (ground)
        {
            if (Random.value < 0.5)
            {
                return MapElements.Water_Top;
            }
            return MapElements.Spikes;
        }
        else
        {
            if (Random.value < 0.5)
            {
                return MapElements.Bridge;
            }
            return MapElements.Ground_Top;
        }
    }

    private bool EnemyPlacement()
    {
        bool EnemySpawned = EnemyPlacement1.PlaceEnemy(new Vector2(_blockNum, _blockHeight + TileSpawnOffset.y));
        return EnemySpawned;
    }

    private void PointPlacement()
    {
        PointsPlacement.PlaceObject(new Vector2(_blockNum, _blockHeight + 2.5f));
    }

    private void PowerUpPlacement()
    {
        PowerUpPlacement1.PlaceObject(new Vector2(_blockNum, _blockHeight + 1.5f));
    }
    private void ObjectPlacement(int tiles, int platformSize, bool isEnemyOnPlatform)
    {
        if (tiles > 1 && tiles < platformSize - 1)
        {
            float rnd = Random.value;
            if (rnd < 0.5f)
            {
                ObjectPlacement1.PlaceObject(new Vector3(_blockNum, _blockHeight, 1));
            }
            else
            {
                InteractiveObjectPlacement.PlaceObject(new Vector2(_blockNum, _blockHeight + TileSpawnOffset.y), isEnemyOnPlatform);
            }
        }
    }

    private void CreateGarbageCollector()
    {
        MapElementsDetails mp = PlatformsProperties.MapElements.Find(x => x.id == MapElements.Garbage_Collector);
        MapElement prefab = mp.mapElement;
        MapElement spawn = prefab.GetPooledInstance<MapElement>();
        spawn.transform.position = new Vector2(_blockNum, GarbageCollectorHeight);
        spawn.transform.rotation = Quaternion.identity;
        spawn.transform.localScale = new Vector2((PlatformsProperties.MaxAmount * PlatformsProperties.MaxSize) / 1.5f, 15.0f);
        spawn.gameObject.SetActive(true);
    }

    private MapElement SpawnElement(MapElements obj, Vector3 position)
    {
        MapElementsDetails mp = PlatformsProperties.MapElements.Find(x => x.id == obj);
        MapElement prefab = mp.mapElement;
        MapElement spawn = prefab.GetPooledInstance<MapElement>();
        spawn.transform.position = position;
        spawn.transform.rotation = Quaternion.identity;
        spawn.gameObject.SetActive(true);
        return spawn;
    }

    private void GeneratePrefabs()
    {
        for(int i=0; i < PlatformsProperties.MapElements.Count; i++)
        {
            MapElement prefab = PlatformsProperties.MapElements[i].mapElement;
            for (int j = 0; j < PlatformsProperties.MapElements[i].preLoadCount; j++)
            {
                MapElement obj = prefab.GetPooledInstance<MapElement>();
               // obj.gameObject.SetActive(true);
            }
        }
    }

    private void SpawnTileWithGround(MapElements mapElement, TileType type)
    {
        SpawnElement(mapElement, new Vector2(_blockNum, _blockHeight));
        GenerateGround(0, type);
    }

    #endregion

    #region Public Methods

    public void GenerateMap()
    {
        for (int platform = 1; platform < PlatformsProperties.MaxAmount; platform++)
        {
            ChoosePlatform(platform);
            if (platform == MapGenerationStarter)
            {
                SpawnElement(MapElements.Map_Generation_Starter, new Vector2(_blockNum, _blockHeight));
                CreateGarbageCollector();
            }
        }
    }

    public void Restart()
    {
        _blockNum = 1;
        _blockHeight = 2;
        GenerateStartPoint();
        PlayerControl prefab = Player;
        PlayerControl spawn = prefab.GetPooledInstance<PlayerControl>();
        spawn.transform.position = new Vector3(-StartSize + 1, StartPlatformCords.y + 1, 0);
        spawn.transform.rotation = Quaternion.identity;
        spawn.gameObject.SetActive(true);
    }

    #endregion

    [System.Serializable]
    public class PlatformProperties
    {

        #region Fields

        [SerializeField]
        private List<MapElementsDetails> _mapElements;
        [SerializeField]
        private int _minSize = 1;
        [SerializeField]
        private int _maxSize = 10;
        [SerializeField]
        private int _maxAmount = 100;
        [SerializeField]
        private int _maxHeight = 3;
        [SerializeField]
        private int _maxDrop = -3;
        [SerializeField]
        private int _groundHeight = 5;
        [SerializeField]
        [Range(0.0f, 1f)]
        private float _bridgeChance = .1f;
        [SerializeField]
        [Range(0.0f, 1f)]
        private float _columnsChance = .1f;

        #endregion

        #region Properties
        public List<MapElementsDetails> MapElements
        {
            get
            {
                return _mapElements;
            }

            set
            {
                _mapElements = value;
            }
        }

        public int MinSize
        {
            get
            {
                return _minSize;
            }

            set
            {
                _minSize = value;
            }
        }

        public int MaxSize
        {
            get
            {
                return _maxSize;
            }

            set
            {
                _maxSize = value;
            }
        }

        public int MaxAmount
        {
            get
            {
                return _maxAmount;
            }

            set
            {
                _maxAmount = value;
            }
        }

        public int MaxHeight
        {
            get
            {
                return _maxHeight;
            }

            set
            {
                _maxHeight = value;
            }
        }

        public int MaxDrop
        {
            get
            {
                return _maxDrop;
            }

            set
            {
                _maxDrop = value;
            }
        }

        public int GroundHeight
        {
            get
            {
                return _groundHeight;
            }

            set
            {
                _groundHeight = value;
            }
        }

        public float BridgeChance
        {
            get
            {
                return _bridgeChance;
            }

            set
            {
                _bridgeChance = value;
            }
        }

        public float ColumnsChance
        {
            get
            {
                return _columnsChance;
            }

            set
            {
                _columnsChance = value;
            }
        }

        #endregion

    }

    [System.Serializable]
     public class HoleProperties
    {
        #region Fields

        [SerializeField]
        private int _minSize = 1;
        [SerializeField]
        private int _maxSize = 3;
        [SerializeField]
        private int _spikesOffset = 2;
        [Range(0.0f, 1f)]
        [SerializeField]
        private float _holeChance = .5f;
        [Range(0.0f, 1f)]
        [SerializeField]
        private float _spikeChance = .5f;

        #endregion

        #region Properties

        public int MinSize
        {
            get
            {
                return _minSize;
            }

            set
            {
                _minSize = value;
            }
        }

        public int MaxSize
        {
            get
            {
                return _maxSize;
            }

            set
            {
                _maxSize = value;
            }
        }

        public int SpikesOffset
        {
            get
            {
                return _spikesOffset;
            }

            set
            {
                _spikesOffset = value;
            }
        }

        public float HoleChance
        {
            get
            {
                return _holeChance;
            }

            set
            {
                _holeChance = value;
            }
        }

        public float SpikeChance
        {
            get
            {
                return _spikeChance;
            }

            set
            {
                _spikeChance = value;
            }
        }

        #endregion

    }

}


