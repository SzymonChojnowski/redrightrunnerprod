﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacement : MonoBehaviour
{
    [SerializeField]
    private int _pooledObjects = 5;
    [SerializeField]
    private List<MapObject> _objects = new List<MapObject>();
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _objectChance = 0.3f;

    #region Properties

    public int PooledObjects
    {
        get
        {
            return _pooledObjects;
        }

        set
        {
            _pooledObjects = value;
        }
    }

    public List<MapObject> Objects
    {
        get
        {
            return _objects;
        }

        set
        {
            _objects = value;
        }
    }

    public float ObjectChance
    {
        get
        {
            return _objectChance;
        }

        set
        {
            _objectChance = value;
        }
    }

    #endregion

    public void PlacePreObjects()
    {
        for (int i = 0; i < PooledObjects; i++)
        {
            for (int j = 0; j < Objects.Count; j++)
            {
                MapObject prefab = Objects[j];
                prefab.GetPooledInstance<MapObject>();
                prefab.gameObject.SetActive(true);
            }
        }
    }

    public void PlaceObject(Vector3 cords)
    {
        if (Random.value < ObjectChance)
        {
            MapObject prefab = Objects[Random.Range(0, Objects.Count)];
            MapObject spawn = prefab.GetPooledInstance<MapObject>();

            spawn.transform.position = new Vector3(cords.x, cords.y + prefab.Y, prefab.Z);
            spawn.transform.rotation = Quaternion.identity;
            spawn.gameObject.SetActive(true);
        }
    }
}
