﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpPlacement : MonoBehaviour
{
    [Range(0.0f, 1.0f)]
    [SerializeField]
    private float _powerUpChance = 0.3f;
    [SerializeField]
    private int _pooledPowerUps = 5;
    [SerializeField]
    private List<PowerUp> _powerUps = new List<PowerUp>();

    #region Properties

    public float PowerUpChance
    {
        get
        {
            return _powerUpChance;
        }

        set
        {
            _powerUpChance = value;
        }
    }

    public int PooledPowerUps
    {
        get
        {
            return _pooledPowerUps;
        }

        set
        {
            _pooledPowerUps = value;
        }
    }

    public List<PowerUp> PowerUps
    {
        get
        {
            return _powerUps;
        }

        set
        {
            _powerUps = value;
        }
    }

    #endregion

    public void PlacePreObjects()
    {
        for (int i = 0; i < PooledPowerUps; i++)
        {
            for (int j = 0; j < PowerUps.Count; j++)
            {
                PowerUp prefab = PowerUps[j];
                prefab.GetPooledInstance<PowerUp>();
                prefab.gameObject.SetActive(true);
            }
        }
    }

    public void PlaceObject(Vector2 cords)
    {
        if (Random.value < PowerUpChance)
        {
            PowerUp prefab = PowerUps[Random.Range(0, PowerUps.Count)];
            PowerUp spawn = prefab.GetPooledInstance<PowerUp>();
            spawn.transform.position = new Vector3(cords.x, cords.y, 0);
            spawn.transform.rotation = Quaternion.identity;
            spawn.gameObject.SetActive(true);
        }
    }
}
