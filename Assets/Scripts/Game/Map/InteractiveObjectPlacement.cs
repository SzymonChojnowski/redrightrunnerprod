﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InteractiveObjectPlacement : MonoBehaviour
{
    [SerializeField]
    private int _pooledObjects = 7;
    [SerializeField]
    private List<InteractiveMapObject> _objects = new List<InteractiveMapObject>();
    [SerializeField]
    [Range(0.0f, 1.0f)]
    private float _objectChance = 0.7f;

    #region Properties

    public int PooledObjects
    {
        get
        {
            return _pooledObjects;
        }

        set
        {
            _pooledObjects = value;
        }
    }

    public List<InteractiveMapObject> Objects
    {
        get
        {
            return _objects;
        }

        set
        {
            _objects = value;
        }
    }

    public float ObjectChance
    {
        get
        {
            return _objectChance;
        }

        set
        {
            _objectChance = value;
        }
    }

    #endregion

    public void PlacePreObjects()
    {
        for (int i = 0; i < PooledObjects; i++)
        {
            for (int j = 0; j < Objects.Count; j++)
            {
                InteractiveMapObject prefab = Objects[j];
                prefab.GetPooledInstance<InteractiveMapObject>();
                prefab.gameObject.SetActive(true);
            }
        }
    }

    public void PlaceObject(Vector2 cords, bool isEnemy)
    {
        if (!isEnemy && Random.value < ObjectChance)
        {
            InteractiveMapObject prefab = Objects[Random.Range(0, Objects.Count)];
            InteractiveMapObject spawn = prefab.GetPooledInstance<InteractiveMapObject>();

            spawn.transform.position = new Vector3(cords.x, cords.y, 0);
            spawn.transform.rotation = Quaternion.identity;
            spawn.gameObject.SetActive(true);
        }
    }
}
