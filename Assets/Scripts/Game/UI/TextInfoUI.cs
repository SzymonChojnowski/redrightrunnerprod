﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextInfoUI : MonoBehaviour
{
    [SerializeField]
    private float _uiTextDisplayDuration = 5f;
    [SerializeField]
    private TextMeshProUGUI _uiText;
    [SerializeField]
    private TextMeshProUGUI _uiSubText;
    [SerializeField]
    private CanvasGroup _infoUI;

    public float UiTextDisplayTimer { get; set; }

    public TextMeshProUGUI UiText
    {
        get
        {
            return _uiText;
        }

        set
        {
            _uiText = value;
        }
    }

    public TextMeshProUGUI UiSubText
    {
        get
        {
            return _uiSubText;
        }

        set
        {
            _uiSubText = value;
        }
    }

    public CanvasGroup InfoUI
    {
        get
        {
            return _infoUI;
        }

        set
        {
            _infoUI = value;
        }
    }

    public float UiTextDisplayDuration
    {
        get
        {
            return _uiTextDisplayDuration;
        }

        set
        {
            _uiTextDisplayDuration = value;
        }
    }

    private void Start()
    {
        UiTextDisplayTimer = UiTextDisplayDuration * 2;
    }

    public void SetUIText(string mainText, string subText, int multiplayer = 1)
    {
        UiTextDisplayTimer = UiTextDisplayDuration * multiplayer;
        UiText.text = mainText;
        UiSubText.text = subText;
    }

    public void FadeTimer()
    {
        UiTextDisplayTimer -= Time.deltaTime;
        if (UiTextDisplayTimer < 1)
        {
            InfoUI.alpha = UiTextDisplayTimer;
        }
        else if (UiTextDisplayTimer < 0)
        {
            InfoUI.alpha = 0f;
        }
        else
        {
            InfoUI.alpha = 1f;
        }
    }

    private void Update()
    {
        FadeTimer();
    }

}
