﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeathScore : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _playedTime;
    [SerializeField]
    private TextMeshProUGUI _powerUps;
    [SerializeField]
    private TextMeshProUGUI _level;
    [SerializeField]
    private TextMeshProUGUI _boxesDestroyed;
    [SerializeField]
    private TextMeshProUGUI _bridgesDestroyed;
    [SerializeField]
    private TextMeshProUGUI _spikes_water;
    [SerializeField]
    private TextMeshProUGUI _monstersKilled;
    [SerializeField]
    private TextMeshProUGUI _monstersKills;
    [SerializeField]
    private TextMeshProUGUI _coinsCollected;
    [SerializeField]
    private TextMeshProUGUI _score;

    public Button restartButton;

    #region Properties
    public TextMeshProUGUI PlayedTime
    {
        get
        {
            return _playedTime;
        }

        set
        {
            _playedTime = value;
        }
    }

    public TextMeshProUGUI PowerUps
    {
        get
        {
            return _powerUps;
        }

        set
        {
            _powerUps = value;
        }
    }

    public TextMeshProUGUI Level
    {
        get
        {
            return _level;
        }

        set
        {
            _level = value;
        }
    }

    public TextMeshProUGUI BoxesDestroyed
    {
        get
        {
            return _boxesDestroyed;
        }

        set
        {
            _boxesDestroyed = value;
        }
    }

    public TextMeshProUGUI BridgesDestroyed
    {
        get
        {
            return _bridgesDestroyed;
        }

        set
        {
            _bridgesDestroyed = value;
        }
    }

    public TextMeshProUGUI Spikes_water
    {
        get
        {
            return _spikes_water;
        }

        set
        {
            _spikes_water = value;
        }
    }

    public TextMeshProUGUI MonstersKilled
    {
        get
        {
            return _monstersKilled;
        }

        set
        {
            _monstersKilled = value;
        }
    }

    public TextMeshProUGUI MonstersKills
    {
        get
        {
            return _monstersKills;
        }

        set
        {
            _monstersKills = value;
        }
    }

    public TextMeshProUGUI CoinsCollected
    {
        get
        {
            return _coinsCollected;
        }

        set
        {
            _coinsCollected = value;
        }
    }

    public TextMeshProUGUI Score
    {
        get
        {
            return _score;
        }

        set
        {
            _score = value;
        }
    }
    #endregion

    void Start()
    {
        gameObject.SetActive(false);
    }

    public void ToggleEndMenu()
    {
        SetStats();
        gameObject.SetActive(true);
    }

    private void SetStats()
    {
        PlayedTime.SetText(GameData.Instance.ConvertTime());
        PowerUps.SetText(GameData.Instance.powerUps.ToString());
        Level.SetText(GameData.Instance.level.ToString());
        BoxesDestroyed.SetText(GameData.Instance.boxesDestroyed.ToString());
        BridgesDestroyed.SetText(GameData.Instance.bridgesDestroyed.ToString());
        Spikes_water.SetText(GameData.Instance.spikes_water.ToString()+ " times");
        MonstersKilled.SetText(GameData.Instance.monstersKilled.ToString());
        MonstersKills.SetText(GameData.Instance.monstersKills.ToString() +" times");
        CoinsCollected.SetText(GameData.Instance.coinsCollected.ToString());
        Score.SetText(GameData.Instance.score.ToString());
    }
}
