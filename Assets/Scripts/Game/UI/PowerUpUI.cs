﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpUI : MonoBehaviour, IPowerUpEvents
{
    [SerializeField]
    private TextMeshProUGUI _uiTextActivePowerups;
    [SerializeField]
    private TextInfoUI _textInfoUI;
    public List<PowerUp> EnablePowerUps { get; set; }
    public PowerUp UseablePowerUp { get; set; }

    public TextMeshProUGUI UiTextActivePowerups
    {
        get
        {
            return _uiTextActivePowerups;
        }

        set
        {
            _uiTextActivePowerups = value;
        }
    }

    [SerializeField]
    private Button useButton;

    private void Awake()
    {
        EnablePowerUps = new List<PowerUp>();
    }

    public void Clear()
    {
        EnablePowerUps.Clear();
        UpdateEnabledPowerUps();
    }

    private void UpdateEnabledPowerUps()
    {
        UiTextActivePowerups.text = "Active Power Ups";

        if (EnablePowerUps == null || EnablePowerUps.Count == 0)
        {
            UiTextActivePowerups.text += "\nNone";
            return;
        }

        foreach (PowerUp powerUp in EnablePowerUps)
        {
            UiTextActivePowerups.text += "\n" + powerUp.PowerUpName;
        }
    }

    void IPowerUpEvents.OnPowerUpCollected(PowerUp powerUp, PlayerControl player)
    {
        if (powerUp.IsUseable && !useButton.interactable)
        {
            useButton.interactable = true;
            UseablePowerUp = powerUp;
            _textInfoUI.SetUIText(powerUp.PowerUpName, powerUp.PowerUpExplanation);
            GameData.Instance.powerUps += 1;
        }
        else
        {
            if (!EnablePowerUps.Exists(element => element.PowerUpName.Equals(powerUp.PowerUpName)) && !powerUp.IsUseable)
            {
                EnablePowerUps.Add(powerUp);
                UpdateEnabledPowerUps();
                _textInfoUI.SetUIText(powerUp.PowerUpName, powerUp.PowerUpExplanation);
                GameData.Instance.powerUps += 1;
            }
        }
    }

    void IPowerUpEvents.OnPowerUpExpired(PowerUp powerUp, PlayerControl player)
    {
        if (powerUp.IsUseable)
        {
            useButton.interactable = false;
        }
        else
        {
            EnablePowerUps.Remove(powerUp);
            UpdateEnabledPowerUps();
        }
    }
}
