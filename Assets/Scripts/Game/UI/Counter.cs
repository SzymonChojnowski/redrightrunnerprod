﻿using TMPro;
using UnityEngine;

public class Counter : MonoBehaviour
{
    private int _amount;

    public int Amount
    {
        get
        {
            return _amount;
        }
        set
        {
            AmountChanged(value);
            _amount = value;
        }
    }
    public TextMeshProUGUI CounterText { get; set; }

    private void AmountChanged(int newValue)
    {
        CounterText.SetText(newValue.ToString());
    }

    void Start()
    {
        CounterText = GetComponentInParent<TextMeshProUGUI>();
    }
}
