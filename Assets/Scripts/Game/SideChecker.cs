﻿using UnityEngine;

public class SideChecker : Singleton<SideChecker>
{
    public enum Side
    {
        left,
        right,
        up,
        down,
        none
    }

    public Side CheckSide(ContactPoint2D[] contacts)
    {
        Side side = Side.none;
        foreach (ContactPoint2D hitPos in contacts)
        {
            if (hitPos.normal.x == 0)
            {
                side = (hitPos.normal.y < 0) ? Side.up : Side.down;
            }
            if (hitPos.normal.y == 0)
            {
                side = (hitPos.normal.x < 0) ? Side.right : Side.left;
            }
        }
        return side;
    }
}
