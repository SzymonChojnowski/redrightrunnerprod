﻿using System.Collections;
using UnityEngine;

public class SinusFly : Enemy
{
    [SerializeField]
    private float _moveSpeed = 5f;
    [SerializeField]
    private float _frequency = 20f;
    [SerializeField]
    private float _magnitude = 0.5f;

    #region Properties

    public Vector3 Pos { get; set; }
    public bool IsDead { get; set; }
    public bool FromEnable { get; set; }
    public BoxCollider2D Coll { get; set; }

    public float MoveSpeed
    {
        get
        {
            return _moveSpeed;
        }

        set
        {
            _moveSpeed = value;
        }
    }

    public float Frequency
    {
        get
        {
            return _frequency;
        }

        set
        {
            _frequency = value;
        }
    }

    public float Magnitude
    {
        get
        {
            return _magnitude;
        }

        set
        {
            _magnitude = value;
        }
    }

    #endregion

    private void Awake()
    {
        Coll = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        Pos = transform.localPosition;
    }

    private void OnEnable()
    {
        FromEnable = true;
        Coll.enabled = true;
        IsDead = false;
    }

    void Update()
    {
        if (!IsDead)
        {
            if (FromEnable)
            {
                Pos = transform.position;
                FromEnable = false;
            }
            Pos -= transform.right * Time.deltaTime * MoveSpeed;
            transform.position = Pos + transform.up * Mathf.Sin(Time.time * Frequency) * Magnitude;
        }
        else
        {
            transform.position -= transform.up * Time.deltaTime * MoveSpeed * 3;
        }
    }

    new protected void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            if (collision.otherCollider.usedByEffector)
            {
                GameController.Instance.mobCounter.Amount += 1;
                StartCoroutine(Die());
            }
            else
            {
                Coll.enabled = false;
                StartCoroutine(Hide());
            }
        }
    }

    private IEnumerator Hide()
    {
        yield return new WaitForSeconds(0.5f);
        Coll.enabled = true;
    }

    private IEnumerator Die()
    {
        IsDead = true;
        yield return new WaitForSeconds(2f);
        IsDead = false;
        ReturnToPool();
    }
}
