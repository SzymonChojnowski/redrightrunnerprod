﻿using System.Collections;
using UnityEngine;

public class Jumper : Enemy
{

    [SerializeField]
    private float _jumpDistance = 300f;
    [SerializeField]
    private float _jumpTime = 4f;
    [SerializeField]
    private float _jumpSpeed = 1f;
    [SerializeField]

    private float _playerDetectRange = 10f;
    private float _jumpTimer, _speedTimer;

    #region Properties

    public GameObject Player { get; set; }
    public bool HasJumped { get; set; }
    public bool IsDead { get; set; }
    public BoxCollider2D Coll { get; set; }

    public Animator Anim { get; set; }
    public SpriteRenderer Render { get; set; }
    public Rigidbody2D Body { get; set; }

    public float JumpDistance
    {
        get
        {
            return _jumpDistance;
        }

        set
        {
            _jumpDistance = value;
        }
    }

    public float JumpTime
    {
        get
        {
            return _jumpTime;
        }

        set
        {
            _jumpTime = value;
        }
    }

    public float JumpSpeed
    {
        get
        {
            return _jumpSpeed;
        }

        set
        {
            _jumpSpeed = value;
        }
    }

    public float PlayerDetectRange
    {
        get
        {
            return _playerDetectRange;
        }

        set
        {
            _playerDetectRange = value;
        }
    }

    #endregion

    void Awake()
    {
        Body = GetComponent<Rigidbody2D>();
        Render = GetComponent<SpriteRenderer>();
        Anim = GetComponent<Animator>();
        Coll = GetComponent<BoxCollider2D>();
    }

    private void OnEnable()
    {
        Coll.enabled = true;
        IsDead = false;
        Anim.SetBool("move", false);
        Anim.SetBool("dead", false);
        Anim.SetBool("hit", false);
    }

    private void Start()
    {
        Player = GameObject.Find("Player(Clone)");
        Anim.keepAnimatorControllerStateOnDisable = true;
    }

    void Update()
    {
        if (!IsDead)
        {
            _jumpTimer += Time.deltaTime;
            if (_jumpTimer >= JumpTime) { HasJumped = true; }
        }
        if (HasJumped)
        {
            Anim.SetBool("move", true);
            HandleJump();
        }
        if (IsDead)
        {
            Anim.SetBool("dead", true);
            Die();
        }
    }

    private void HandleJump()
    {
        if (!IsDead && Player != null)
        {
            Vector2 direction = transform.position - Player.transform.position;
            float distance = Vector2.Distance(transform.position, Player.transform.position);
            _speedTimer += Time.deltaTime;
            Jump(direction, distance);
        }
    }

    private void Jump(Vector2 direction, float distance)
    {
        if (_speedTimer >= JumpSpeed)
        {
            Body.AddForce(Vector2.up * JumpDistance);
            if (direction.x > 0 && distance < PlayerDetectRange)
            {
                JumpOnSide(Vector2.left, false);
            }
            else if (direction.x < 0 && distance < PlayerDetectRange)
            {
                JumpOnSide(Vector2.right, true);
            }
            else
            {
                if (Random.value > .50f)
                {
                    JumpOnSide(Vector2.left, false);
                }
                else
                {
                    JumpOnSide(Vector2.right, true);
                }
            }
            EndJump();
        }
    }

    private void JumpOnSide(Vector2 direction, bool shouldFlip)
    {
        Body.AddForce(direction * (JumpDistance / 3));
        Render.flipX = shouldFlip;
    }

    private void EndJump()
    {
        Anim.SetBool("move", false);
        _speedTimer = 0;
        _jumpTimer = 0;
        HasJumped = false;
    }

    private void Die()
    {
        Coll.enabled = false;
        IsDead = false;
        StartCoroutine(TurnOnColliders());
    }

    private IEnumerator TurnOnColliders()
    {
        yield return new WaitForSeconds(1.5f);
        Coll.enabled = true;
        Anim.SetBool("move", false);
        Anim.SetBool("dead", false);
        Anim.SetBool("hit", false);
        ReturnToPool();
    }

    new protected void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            if (collision.otherCollider.usedByEffector)
            {
                Anim.SetBool("hit", true);
                IsDead = true;
                GameController.Instance.mobCounter.Amount += 1;
            }
        }
    }
}