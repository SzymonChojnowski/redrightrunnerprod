﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : PowerUp
{
    [Header("Power Up Abilities")]
    [SerializeField]
    private float _power = 5f;

    #region Properties

    public float Power
    {
        get
        {
            return _power;
        }

        set
        {
            _power = value;
        }
    }

    #endregion

    protected override void PowerUpPayload(PlayerControl playerControl)
    {
        base.PowerUpPayload(playerControl);
        if (PlayerControl.IsMagnet == true)
        {
            return;
        }
        PlayerControl.IsMagnet = true;
    }

    protected override void PowerUpHasExpired()
    {
        base.PowerUpHasExpired();
        if (State == PowerUpState.IsExpiring)
        {
            return;
        }
        PlayerControl.IsMagnet = false;
    }

    private void Update()
    {
        if (State == PowerUpState.IsCollected)
        {
            Duration -= Time.deltaTime;
            if (Duration < 0)
            {
                PowerUpHasExpired();
            }
        }
    }
}
