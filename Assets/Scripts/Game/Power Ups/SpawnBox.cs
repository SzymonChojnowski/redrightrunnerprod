﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBox : PowerUp {

    [Header("Power Up Abilities")]
    [SerializeField]
    private InteractiveMapObject _box;

    #region Properties

    public InteractiveMapObject Box
    {
        get
        {
            return _box;
        }

        set
        {
            _box = value;
        }
    }

    #endregion

    protected override void PowerUpPayload(PlayerControl playerControl)
    {
        base.PowerUpPayload(playerControl);
    }

    protected override void PowerUpHasExpired()
    {
        base.PowerUpHasExpired();
    }

    protected override void PowerUpUsage()
    {
        base.PowerUpUsage();
        InteractiveMapObject spawn = Box.GetPooledInstance<InteractiveMapObject>();
        if(PlayerControl.State == BasePlayerState.STATE_JUMPING || PlayerControl.State == BasePlayerState.STATE_FALLING)
        {
            spawn.transform.position = PlayerControl.transform.position + new Vector3(0.2f, -1);
        }
        else
        {
            spawn.transform.position = PlayerControl.transform.position + new Vector3(1, 0);
        }
        spawn.transform.rotation = Quaternion.identity;
        spawn.gameObject.SetActive(true);
        PowerUpHasExpired();
    }
}
