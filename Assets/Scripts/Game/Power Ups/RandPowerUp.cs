﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandPowerUp : PowerUp
{
    [Header("Power Up Abilities")]
    [SerializeField]
    private List<PowerUp> _availableToRand;

    #region Properties
    public List<PowerUp> AvailableToRand
    {
        get
        {
            return _availableToRand;
        }

        set
        {
            _availableToRand = value;
        }
    }
    #endregion

    protected override void PowerUpPayload(PlayerControl playerControl)
    {
        base.PowerUpPayload(playerControl);
    }

    protected override void PowerUpHasExpired()
    {
        base.PowerUpHasExpired();
    }

    protected override void PowerUpUsage()
    {
        base.PowerUpUsage();
        PowerUp spawn = ChoosePowerUp().GetPooledInstance<PowerUp>();
        spawn.transform.position = PlayerControl.transform.position + new Vector3(0, 0);
        spawn.transform.rotation = Quaternion.identity;
        spawn.gameObject.SetActive(true);
        PowerUpHasExpired();
    }
    private PowerUp ChoosePowerUp()
    {
        return AvailableToRand[Random.Range(0, AvailableToRand.Count)];
    }
}
