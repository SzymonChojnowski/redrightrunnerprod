﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multiplier : PowerUp {

    [Header("Power Up Abilities")]
    [SerializeField]
    private int _minMultiplayer = 2;
    [SerializeField]
    private int _maxMultiplayer = 3;

    #region Properties

    public int MinMultiplayer
    {
        get
        {
            return _minMultiplayer;
        }

        set
        {
            _minMultiplayer = value;
        }
    }

    public int MaxMultiplayer
    {
        get
        {
            return _maxMultiplayer;
        }

        set
        {
            _maxMultiplayer = value;
        }
    }

    #endregion

    protected override void PowerUpPayload(PlayerControl playerControl)
    {
        base.PowerUpPayload(playerControl);
        if (playerControl.CoinMultiplayer > 1)
        {
            return;
        }
        playerControl.CoinMultiplayer = Random.Range(MinMultiplayer, MaxMultiplayer);
    }
   
    protected override void PowerUpHasExpired()
    {
        base.PowerUpHasExpired();
        if (State == PowerUpState.IsExpiring)
        {
            return;
        }
        PlayerControl.CoinMultiplayer = 1;
    }

    private void Update()
    {
        if (State == PowerUpState.IsCollected)
        {
            Duration -= Time.deltaTime;
            if (Duration < 0)
            {
                PowerUpHasExpired();
            }
        }
    }
}
