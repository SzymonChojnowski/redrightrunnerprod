﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GodMode : PowerUp {
    protected override void PowerUpPayload(PlayerControl playerControl)
    {
        base.PowerUpPayload(playerControl);
        if (playerControl.isInvulnerability)
        {
            return;
        }
        playerControl.isInvulnerability = true;
    }

    protected override void PowerUpHasExpired()
    {
        if (State == PowerUpState.IsExpiring)
        {
            return;
        }
        PlayerControl.isInvulnerability = false;
        base.PowerUpHasExpired();
    }

    private void Update()
    {
        if (State == PowerUpState.IsCollected)
        {
            Duration -= Time.deltaTime;
            if (Duration < 0)
            {
                PowerUpHasExpired();
            }
        }
    }
}
