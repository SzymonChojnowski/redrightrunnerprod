﻿using System.Collections.Generic;
using UnityEngine;

public class GameData : Singleton<GameData>
{
    public int playedTime;
    public int powerUps;
    public int level;
    public int boxesDestroyed;
    public int bridgesDestroyed;
    public int spikes_water;
    public int monstersKilled;
    public int monstersKills;
    public int coinsCollected;
    public int score;

    private string playedTimeinMin;

    public Dictionary<string, object> ToDictionary()
    {
        Dictionary<string, object> result = new Dictionary<string, object>();
        result["boxesDestroyed"] = boxesDestroyed;
        result["bridgesDestroyed"] = bridgesDestroyed;
        result["coinsCollected"] = coinsCollected;
        result["level"] = level;
        result["monstersKilled"] = monstersKilled;
        result["monstersKills"] = monstersKills;
        result["playedTime"] = playedTime;
        result["powerUps"] = powerUps;
        result["score"] = score;
        result["spikes_water"] = spikes_water;
        return result;
    }

    void Start()
    {
        Restart();
    }

    public void Restart()
    {
        playedTime = 0;
        powerUps = 0;
        level = 0;
        boxesDestroyed = 0;
        bridgesDestroyed = 0;
        spikes_water = 0;
        monstersKilled = 0;
        monstersKills = 0;
        coinsCollected = 0;
    }

    public string ConvertTime()
    {
        int seconds = playedTime % 60;
        int minutes = (playedTime / 60) % 60;
        if (seconds <= 9)
        {
          playedTimeinMin = minutes + ":" + "0" + seconds;
        }
        else
        {
            playedTimeinMin = minutes + ":" + seconds;
        }
        return playedTimeinMin;
    }
}


