﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : BaseState
{

    public override void OnUpdate()
    {

    }

    public override void OnEnter(GameController controller)
    {
        base.OnEnter(controller);
        controller.UI.SetActive(true);
        controller.staticUI.SetActive(false);
        controller.refUI.SetActive(false);

    }

    public override void OnExit(GameController controller)
    {
        controller.UI.SetActive(false);
        controller.staticUI.SetActive(true);
        controller.refUI.SetActive(true);
        base.OnExit(controller);
    }

    public override void HandleInput(GameController controller)
    {

    }
}
