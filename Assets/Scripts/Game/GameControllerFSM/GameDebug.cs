﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDebug : BaseState {

    protected EnemyPlacement enemyPlacement;

    public override void HandleInput(GameController controller)
    {
        if(Input.GetKeyUp("1"))
        {
            Vector2 enemyPos = new Vector2(controller.MapGenerator.Player.transform.position.x + 3, controller.MapGenerator.Player.transform.position.y + 1);
            enemyPlacement.PlaceEnemy(enemyPos);
        }
    }

    public override void OnEnter(GameController controller)
    {
        enemyPlacement = GameObject.Find("EnemyPlacement").GetComponent<EnemyPlacement>();
        base.OnExit(controller);
    }

    public override void OnUpdate()
    {

    }

}
