﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameStates
{
    void OnUpdate();

    void OnEnter(GameController controller);

    void OnExit(GameController controller);

    void ToState(GameController controller, IGameStates targetState);

    void HandleInput(GameController controller);

}
