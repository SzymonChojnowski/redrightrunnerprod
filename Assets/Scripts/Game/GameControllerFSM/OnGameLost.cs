﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class OnGameLost : BaseState
{

    public override void OnUpdate()
    {

    }

    public override void OnEnter(GameController controller)
    {
        base.OnEnter(controller);
        controller.UI.SetActive(true);
        GameOverLose(controller);
    }

    public override void OnExit(GameController controller)
    {
        base.OnExit(controller);
        controller.UI.SetActive(false);
        ReloadLevel(controller);
    }

    private void GameOverLose(GameController controller)
    {
        for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
        {
            ExecuteEvents.Execute<IMainGameEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnGameLost());
        }
        GameData.Instance.monstersKilled = controller.mobCounter.Amount;
        GameData.Instance.coinsCollected = controller.coinCounter.Amount;
        GameData.Instance.score = HandleScore(controller.mobCounter, controller.coinCounter);
        controller.DeathScore.ToggleEndMenu();
        if (CommonData.Instance != null && CommonData.Instance.IsSignedIn)
        {
            controller.DatabaseManager.AddScore();
        }
    }

    public void ReloadLevel(GameController controller)
    {
        controller.DeathScore.gameObject.SetActive(false);
        controller.MapGenerator.Restart();
        controller.MapGenerator.GenerateMap();
        GameData.Instance.Restart();
        controller.PowerUpUI.Clear();
        controller.coinCounter.Amount = 0;
        controller.mobCounter.Amount = 0;
        controller.useButton.interactable = false;
        controller.TextInfoUI.SetUIText("Previous Score: " + GameData.Instance.score.ToString(), "Good luck...again", 2);
    }

    private int HandleScore(Counter mobCounter, Counter coinCounter)
    {
        if (mobCounter.Amount <= 1)
        {
            return coinCounter.Amount * 1;
        }
        return coinCounter.Amount * mobCounter.Amount;
    }

    public override void HandleInput(GameController controller)
    {
    }
}
