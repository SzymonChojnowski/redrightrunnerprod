﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : Singleton<GameController>, IPlayerEvents
{
    public Counter coinCounter, mobCounter;
    [SerializeField]
    private TextInfoUI _textInfoUI;
    [SerializeField]
    private PowerUpUI _powerUpUI;
    [SerializeField]
    [Header("Sounds")]
    private AudioClip _soundEffectLose;
    [SerializeField]
    private AudioClip _soundEffectLvlUp;
    [Header("Other")]
    [SerializeField]
    private DeathScore _deathScore;
    [SerializeField]
    private DatabaseManager _databaseManager;
    [SerializeField]
    public Button useButton;
    [SerializeField]
    public GameObject UI;
    public GameObject staticUI;
    public GameObject refUI;


    #region Properties

    public AudioSource AudioSource { get; set; }
    public MapGeneration MapGenerator { get; set; }

    public DeathScore DeathScore
    {
        get
        {
            return _deathScore;
        }

        set
        {
            _deathScore = value;
        }
    }

    public DatabaseManager DatabaseManager
    {
        get
        {
            return _databaseManager;
        }

        set
        {
            _databaseManager = value;
        }
    }

    public TextInfoUI TextInfoUI
    {
        get
        {
            return _textInfoUI;
        }

        set
        {
            _textInfoUI = value;
        }
    }

    public PowerUpUI PowerUpUI
    {
        get
        {
            return _powerUpUI;
        }

        set
        {
            _powerUpUI = value;
        }
    }

    public IGameStates State { get; set; }

    #endregion
    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
        MapGenerator = FindObjectOfType<MapGeneration>();
    }

    void Start()
    {
        State = BaseState.STATE_PLAYING;

        if (SceneManager.GetActiveScene().name == "TestArea")
        {

            State.ToState(this, BaseState.STATE_DEBUG);
            Debug.Log("DEBUG");
        }
        else
        {
            State.ToState(this, BaseState.STATE_MENU);
            useButton.interactable = false;
        }

    }

    public void Update()
    {
        State.OnUpdate();
        State.HandleInput(this);
    }

    public void PlaySound(AudioClip audioClip)
    {
        if (audioClip != null)
        {
            AudioSource.PlayOneShot(audioClip);
        }
    }

    public void ReloadLevel()
    {
        State.ToState(this, BaseState.STATE_PLAYING);
    }

    void IPlayerEvents.OnPlayerReachedGeneration(PlayerControl player)
    {
        GameData.Instance.level += 1;
        PlaySound(_soundEffectLvlUp);
        TextInfoUI.SetUIText("Level Up ! ! !", "LEVEL " + GameData.Instance.level.ToString());
        MapGenerator.GenerateMap();
    }

    void IPlayerEvents.OnPlayerHurt()
    {
        if (State != BaseState.STATE_DEAD)
        {
            State.ToState(this, BaseState.STATE_DEAD);
            PlaySound(_soundEffectLose);
        }
    }


    public void OnUseClick()
    {
        if (Input.touchCount == 1)
        {
            if (EventSystem.current.currentSelectedGameObject != null)
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Began)
                    {
                        PowerUpUI.UseablePowerUp.Activated = true;
                        
                    }
                }
            }
        }
        PowerUpUI.UseablePowerUp.Activated = true;
    }

    public void OnExitClick()
    {
        State.ToState(this, BaseState.STATE_MENU);
    }

    public void StartGame()
    {
        State.ToState(this, BaseState.STATE_PLAYING);
    }

}
