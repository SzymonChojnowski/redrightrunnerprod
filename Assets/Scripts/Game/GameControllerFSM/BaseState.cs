﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseState : IGameStates
{
    static public readonly IGameStates STATE_MENU = new Menu();
    static public readonly IGameStates STATE_PLAYING = new Playing();
    static public readonly IGameStates STATE_DEAD = new OnGameLost();
    static public readonly IGameStates STATE_DEBUG = new GameDebug();


    public virtual void OnEnter(GameController controller) { }
    public virtual void OnExit(GameController controller) { }
    public virtual void ToState(GameController controller, IGameStates targetState)
    {
        controller.State.OnExit(controller);
        controller.State = targetState;
        controller.State.OnEnter(controller);
        Debug.Log(controller.State.ToString());
    }
    public abstract void OnUpdate();
    public abstract void HandleInput(GameController controller);

}
