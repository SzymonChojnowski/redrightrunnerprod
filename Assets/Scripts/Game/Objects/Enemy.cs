﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : PooledObject
{
    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }

}
