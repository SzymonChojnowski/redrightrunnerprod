﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : Points
{
    private Transform _player;
    private PlayerControl _playerControl;
    private AudioSource _coinSound;
    private SpriteRenderer _sprite;
    private BoxCollider2D _box;

    private void Start()
    {
        _player = GameObject.Find("Player(Clone)").GetComponent<Transform>();
        _playerControl = GameObject.Find("Player(Clone)").GetComponent<PlayerControl>();
    }

    private void Awake()
    {
        _coinSound = GetComponent<AudioSource>();
        _sprite = GetComponent<SpriteRenderer>();
        _box = GetComponent<BoxCollider2D>();
    }
    private void OnEnable()
    {
        _box.enabled = true;
        _sprite.enabled = true;
    }

    void Update()
    {
        if (PlayerControl.IsMagnet)
        {
            if (Vector3.Distance(transform.position, _player.position) < 5)
            {
                transform.position = Vector3.MoveTowards(transform.position, _player.position, 0.1f);
            }
        }
    }

    new private void OnCollisionEnter2D(Collision2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            gameObject.SetActive(false);
        }
    }

    new private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            _coinSound.Play();
            GameController.Instance.coinCounter.Amount += _playerControl.CoinMultiplayer;
            StartCoroutine(HideCoin());
        }
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL) && IsSingle())
        {
            ReturnToPool();
        }
    }

    private IEnumerator HideCoin()
    {
        _sprite.enabled = false;
        _box.enabled = false;
        yield return new WaitForSeconds(0.4f);
        _box.enabled = true;
        _sprite.enabled = true;
        gameObject.SetActive(false);
    }

    private bool IsSingle()
    {
        if (gameObject.transform.parent.tag == "Points")
        {
            return false;
        }
        return true;
    }
}
