﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapObject : PooledObject
{

    [SerializeField]
    private float _y;
    [SerializeField]
    private float _z;

    #region Properties

    public float Y
    {
        get
        {
            return _y;
        }

        set
        {
            _y = value;
        }
    }

    public float Z
    {
        get
        {
            return _z;
        }

        set
        {
            _z = value;
        }
    }

    #endregion

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }
}
