﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MapElement : PooledObject
{
    [SerializeField]
    private bool _shouldDie, _mustDie;
    [SerializeField]
    private bool _destructable;
    [SerializeField]
    private float _fadeInTime = 5f;
    [SerializeField]
    private Color _endColor;
    [SerializeField]
    private int _health = 50;

    #region Properties

    public MeshRenderer Mesh { get; set; }

    public bool ShouldDie
    {
        get
        {
            return _shouldDie;
        }

        set
        {
            _shouldDie = value;
        }
    }

    public bool MustDie
    {
        get
        {
            return _mustDie;
        }

        set
        {
            _mustDie = value;
        }
    }

    public bool Destructable
    {
        get
        {
            return _destructable;
        }

        set
        {
            _destructable = value;
        }
    }

    public float FadeInTime
    {
        get
        {
            return _fadeInTime;
        }

        set
        {
            _fadeInTime = value;
        }
    }

    public Color EndColor
    {
        get
        {
            return _endColor;
        }

        set
        {
            _endColor = value;
        }
    }

    public int Health
    {
        get
        {
            return _health;
        }

        set
        {
            _health = value;
        }
    }

    #endregion

    private void OnEnable()
    {
        Health = 50;
        if (Mesh != null)
        {
            Mesh.material.color = Color.white;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            if (Destructable)
            {
                StartCoroutine("StartDestroying");
            }
            if (ShouldDie)
            {
                if (SideChecker.Instance.CheckSide(collision.contacts) == SideChecker.Side.left)
                {
                    SetPlayerDead();
                }
            }
            if (MustDie)
            {
                SetPlayerDead();
            }
        }
    }

    private void SetPlayerDead()
    {
        GameData.Instance.spikes_water += 1;
        for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
        {
            ExecuteEvents.Execute<IPlayerEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnPlayerHurt());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Map_Remover"))
        {
            ReturnToPool();
        }
    }

    private IEnumerator StartDestroying()
    {
        if (Health <= 0)
        {
            GameData.Instance.bridgesDestroyed += 1;
            Health = 50;
            Mesh.material.color = Color.white;
            ReturnToPool();
            StopAllCoroutines();
        }
        yield return new WaitForFixedUpdate();
        Health -= 1;
        Mesh.material.color = Color.Lerp(Mesh.material.color, EndColor, FadeInTime);
        StartCoroutine("StartDestroying");
    }

    private void Awake()
    {
        Mesh = GetComponent<MeshRenderer>();
    }
}
