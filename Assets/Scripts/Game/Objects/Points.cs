﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Points : PooledObject
{
    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
        else if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.ENEMY))
        {
            ReturnToPool();
        }
        else if (collision.gameObject.CompareTag("Points"))
        {
            ReturnToPool();
        }
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }
}
