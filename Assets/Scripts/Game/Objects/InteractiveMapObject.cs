﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveMapObject : PooledObject
{

    [SerializeField]
    private bool _destructable = false;
    [SerializeField]
    private float _fadeInTime = 0.05f;
    [SerializeField]
    private Color _endColor;

    private int _health = 50;

    #region Properties
    public SpriteRenderer Sprite { get; set; }

    public bool Destructable
    {
        get
        {
            return _destructable;
        }

        set
        {
            _destructable = value;
        }
    }

    public float FadeInTime
    {
        get
        {
            return _fadeInTime;
        }

        set
        {
            _fadeInTime = value;
        }
    }

    public Color EndColor
    {
        get
        {
            return _endColor;
        }

        set
        {
            _endColor = value;
        }
    }

    public int Health
    {
        get
        {
            return _health;
        }

        set
        {
            _health = value;
        }
    }

    #endregion

    private void OnEnable()
    {
        Health = 50;
        if (Sprite != null)
        {
            Sprite.material.color = Color.white;
        }
    }

    private void Start()
    {
        Sprite = GetComponent<SpriteRenderer>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.PLAYER))
        {
            if (Destructable)
            {
                StartCoroutine("StartDestroying");
            }
        }
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Util.Instance.IsInLayerMask(collision.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            ReturnToPool();
        }
    }

    IEnumerator StartDestroying()
    {
        if(Health<=0)
        {
            GameData.Instance.boxesDestroyed += 1;
            Health = 50;
            Sprite.material.color = Color.white;
            ReturnToPool();
            StopAllCoroutines();
        }
        yield return new WaitForFixedUpdate();
        Health -= 1;
        Sprite.material.color = Color.Lerp(Sprite.material.color, EndColor, FadeInTime);
        StartCoroutine("StartDestroying");
    }
}
