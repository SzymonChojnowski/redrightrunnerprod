﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PowerUp : PooledObject
{
    [Header("Power Up Details")]
    [SerializeField]
    private float _duration = 5f;
    private float defDuration;
    [SerializeField]
    private string _powerUpName;
    [SerializeField]
    private string _powerUpExplanation;
    [SerializeField]
    private bool _isUseable;
    [Header("Power Up Resources")]
    [SerializeField]
    private GameObject _specialEffect;
    [SerializeField]
    private AudioClip _soundEffectPickUp;
    [SerializeField]
    private AudioClip _soundEffectPowerDown;
    [SerializeField]
    private Sprite _powerUpImage;

    private bool _activated;

    #region Properties
    protected PowerUpState State { get; set; }
    public PlayerControl PlayerControl { get; set; }
    public Point Coin { get; set; }
    protected SpriteRenderer SpriteRenderer { get; set; }

    public bool Activated
    {
        get
        {
            return _activated;
        }
        set
        {
            UseButtonActivated();
            _activated = value;
        }
    }

    protected float Duration
    {
        get
        {
            return _duration;
        }

        set
        {
            _duration = value;
        }
    }

    public GameObject SpecialEffect
    {
        get
        {
            return _specialEffect;
        }

        set
        {
            _specialEffect = value;
        }
    }

    public AudioClip SoundEffectPickUp
    {
        get
        {
            return _soundEffectPickUp;
        }

        set
        {
            _soundEffectPickUp = value;
        }
    }

    public AudioClip SoundEffectPowerDown
    {
        get
        {
            return _soundEffectPowerDown;
        }

        set
        {
            _soundEffectPowerDown = value;
        }
    }

    protected Sprite PowerUpImage
    {
        get
        {
            return _powerUpImage;
        }

        set
        {
            _powerUpImage = value;
        }
    }

    public string PowerUpName
    {
        get
        {
            return _powerUpName;
        }

        set
        {
            _powerUpName = value;
        }
    }

    public string PowerUpExplanation
    {
        get
        {
            return _powerUpExplanation;
        }

        set
        {
            _powerUpExplanation = value;
        }
    }

    public bool IsUseable
    {
        get
        {
            return _isUseable;
        }

        set
        {
            _isUseable = value;
        }
    }

    #endregion

    private void UseButtonActivated()
    {
        PowerUpUsage();
    }

    protected enum PowerUpState
    {
        InAttractMode,
        IsCollected,
        IsExpiring
    }

    protected virtual void Awake()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
        PowerUpImage = GetComponent<Sprite>();
    }

    protected virtual void Start()
    {
        defDuration = Duration;
        State = PowerUpState.InAttractMode;
    }

    private void OnDisable()
    {
        State = PowerUpState.InAttractMode;
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (Util.Instance.IsInLayerMask(other.gameObject.layer, Util.Instance.RETURN_TO_POOL))
        {
            State = PowerUpState.InAttractMode;
            SpriteRenderer.enabled = true;
            ReturnToPool();
        }
        else
        {
            PowerUpCollected(other.gameObject);
        }
    }

    protected virtual void PowerUpCollected(GameObject obj)
    {
        if (!Util.Instance.IsInLayerMask(obj.layer, Util.Instance.PLAYER))
        {
            return;
        }
        if (State == PowerUpState.IsCollected || State == PowerUpState.IsExpiring)
        {
            return;
        }
        State = PowerUpState.IsCollected;
        PlayerControl = obj.GetComponent<PlayerControl>();
        PowerUpEffects();
        PowerUpPayload(PlayerControl);
        for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
        {
            ExecuteEvents.Execute<IPowerUpEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnPowerUpCollected(this, PlayerControl));
        }
        SpriteRenderer.enabled = false;
    }

    protected virtual void PowerUpEffects()
    {
        GameController.Instance.PlaySound(SoundEffectPickUp);
    }

    protected virtual void PowerUpPayload(PlayerControl playerControl) { }

    protected virtual void PowerUpHasExpired()
    {
        if (State == PowerUpState.IsExpiring)
        {
            return;
        }
        State = PowerUpState.IsExpiring;

        for (int i = 0; i < EventListener.Instance.listeners.Count; ++i)
        {
            ExecuteEvents.Execute<IPowerUpEvents>(EventListener.Instance.listeners[i], null, (x, y) => x.OnPowerUpExpired(this, PlayerControl));
        }
        GameController.Instance.PlaySound(SoundEffectPowerDown);
        SpriteRenderer.enabled = true;
        Duration = defDuration;
        State = PowerUpState.InAttractMode;
        ReturnToPool();
    }

    protected virtual void PowerUpUsage() { }
}
