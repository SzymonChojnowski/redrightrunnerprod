﻿using UnityEngine;
public class BackgroundMove : MonoBehaviour
{
    [SerializeField]
    private float _moveSpeed = 5f;
    [SerializeField]
    private float _frequency = 20f;
    [SerializeField]
    private float _magnitude = 0.5f;

    #region Properties

    public Vector3 LocalScale { get; set; }
    public Vector3 Pos { get; set; }
    public bool FromEnable { get; set; }

    public float MoveSpeed
    {
        get
        {
            return _moveSpeed;
        }

        set
        {
            _moveSpeed = value;
        }
    }

    public float Frequency
    {
        get
        {
            return _frequency;
        }

        set
        {
            _frequency = value;
        }
    }

    public float Magnitude
    {
        get
        {
            return _magnitude;
        }

        set
        {
            _magnitude = value;
        }
    }

    #endregion

    private void Start()
    {
        Pos = transform.localPosition;
    }

    private void OnEnable()
    {
        FromEnable = true;
    }

    void Update()
    {
        if (FromEnable)
        {
            Pos = transform.position;
            FromEnable = false;
        }
        Pos -= transform.right * Time.deltaTime * MoveSpeed;
        transform.position = Pos + transform.up * Mathf.Sin(Time.time * Frequency) * Magnitude;
    }
}
