﻿using Firebase.Auth;
using System;
using System.Collections;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [Header("Login")]
    [SerializeField]
    private TMP_InputField _emailText;
    [SerializeField]
    private TMP_InputField _passwordText;
    [Header("Registration")]
    [SerializeField]
    private TMP_InputField _emailText_reg;
    [SerializeField]
    private TMP_InputField _passwordText_reg;
    [SerializeField]
    private TMP_InputField _nickname_reg;
    [Header("Other")]
    [SerializeField]
    private TextMeshProUGUI _welcomeText;
    [SerializeField]
    private GameObject _deleteQuestion;
    [SerializeField]
    private DatabaseManager _dbManager;
    private string _email = "";
    private string _password = "";
    private string _displayName = "";
    private GameController gameController;

    #region Properties

    public PanelSwitcher PanelSwitcher { get; set; }
    public TMP_InputField EmailText
    {
        get
        {
            return _emailText;
        }
    }

    public TMP_InputField PasswordText
    {
        get
        {
            return _passwordText;
        }
    }

    public TMP_InputField EmailText_reg
    {
        get
        {
            return _emailText_reg;
        }
    }

    public TMP_InputField PasswordText_reg
    {
        get
        {
            return _passwordText_reg;
        }
    }

    public TMP_InputField Nickname_reg
    {
        get
        {
            return _nickname_reg;
        }
    }

    public TextMeshProUGUI WelcomeText
    {
        get
        {
            return _welcomeText;
        }

        set
        {
            _welcomeText = value;
        }
    }

    public GameObject DeleteQuestion
    {
        get
        {
            return _deleteQuestion;
        }
    }

    public DatabaseManager DbManager
    {
        get
        {
            return _dbManager;
        }

        set
        {
            _dbManager = value;
        }
    }

    #endregion



    public void Start()
    {
        gameController = FindObjectOfType<GameController>();
        PanelSwitcher = GetComponent<PanelSwitcher>();
    }

    public void DebugLog(string s)
    {
        Debug.Log(s);
    }

    void VerifyToken()
    {
        DebugLog("Attempting to (Credential) sign in as ...");
        CommonData.Instance.auth.SignInWithCredentialAsync(CommonData.Instance.cred)
          .ContinueWith(HandleSigninResult);
    }

    protected Task GetCredential()
    {
        var tcs = new TaskCompletionSource<bool>();
        if (CommonData.Instance.auth.CurrentUser == null)
        {
            DebugLog("Not signed in, unable to link credential to user.");
            tcs = new TaskCompletionSource<bool>();
            tcs.SetException(new Exception("Not signed in"));
            return tcs.Task;
        }
        CommonData.Instance.cred = EmailAuthProvider.GetCredential(_email, _password);
        tcs.SetException(new Exception("Link credential done"));
        return tcs.Task;
    }

    public bool LogTaskCompletion(Task task, string operation)
    {
        bool complete = false;
        if (task.IsCanceled)
        {
            DebugLog(operation + " canceled.");
        }
        else if (task.IsFaulted)
        {
            DebugLog(operation + " encounted an error.");
            foreach (Exception exception in task.Exception.Flatten().InnerExceptions)
            {

                string authErrorCode = "";
                Firebase.FirebaseException firebaseEx = exception as Firebase.FirebaseException;

                if (firebaseEx != null)
                {

                    authErrorCode = String.Format("AuthError.{0}: ",
                    ((AuthError)firebaseEx.ErrorCode).ToString());
                }
                DebugLog(authErrorCode + exception.ToString());
            }
        }
        else if (task.IsCompleted)
        {
            DebugLog(operation + " completed");
            complete = true;
        }
        return complete;
    }

    public void CreateUserAsync()
    {
        DebugLog(String.Format("Attempting to create user {0}...", _email));
        StartCoroutine(DbManager.IsNickTaken(_displayName, results => onReceivedNick(results)));
    }

    private void onReceivedNick(bool isTaken)
    {
        if (isTaken)
        {
            NotificationManager.Instance.AddNotification(2.5f, "Nick is taken!", Notification.Type.error);
        }
        else
        {
            string newDisplayName = _displayName;
            CommonData.Instance.auth.CreateUserWithEmailAndPasswordAsync(_email, _password)
              .ContinueWith((task) =>
              {
                  return HandleCreateUserAsync(task, newDisplayName: newDisplayName);
              }).Unwrap();
        }
    }

    Task HandleCreateUserAsync(Task<FirebaseUser> authTask, string newDisplayName = null)
    {
        if (LogTaskCompletion(authTask, "User Creation"))
        {
            NotificationManager.Instance.AddNotification(3f, "Registration Successful", Notification.Type.success);
            if (CommonData.Instance.auth.CurrentUser != null)
            {
                DebugLog(String.Format("User Info: {0}  {1}", CommonData.Instance.auth.CurrentUser.Email,
                                       CommonData.Instance.auth.CurrentUser.ProviderId));
                return UpdateUserProfileAsync(newDisplayName: newDisplayName);
            }
        }
        return Task.FromResult(0);
    }

    public Task UpdateUserProfileAsync(string newDisplayName = null)
    {
        if (CommonData.Instance.auth.CurrentUser == null)
        {
            DebugLog("Not signed in, unable to update user profile");
            return Task.FromResult(0);
        }
        _displayName = newDisplayName ?? _displayName;
        DebugLog("Updating user profile");
        return CommonData.Instance.auth.CurrentUser.UpdateUserProfileAsync(new Firebase.Auth.UserProfile
        {
            DisplayName = _displayName,
            PhotoUrl = CommonData.Instance.auth.CurrentUser.PhotoUrl,

        }).ContinueWith(HandleUpdateUserProfile);
    }

    void HandleUpdateUserProfile(Task authTask)
    {
        DbManager.PushUser(CommonData.Instance.auth.CurrentUser.UserId, _email, _displayName);
        if (LogTaskCompletion(authTask, "User profile"))
        {
            Debug.Log("Registration complete");
        }
        SigninAsync();
    }

    public void SigninAsync()
    {
        DebugLog(String.Format("Attempting to sign in as {0}...", _email));
        CommonData.Instance.auth.SignInWithEmailAndPasswordAsync(_email, _password)
          .ContinueWith(HandleSigninResult);
    }

    void HandleSigninResult(Task<FirebaseUser> task)
    {
        if (LogTaskCompletion(task, "Sign-in"))
        {
            CommonData.Instance.user = CommonData.Instance.auth.CurrentUser.UserId;
            CommonData.Instance.IsSignedIn = true;
            StartCoroutine(DbManager.GetNick(results => WelcomeText.SetText("Welcome, " + results)));
            DebugLog(String.Format("{0} signed in", task.Result.DisplayName));
            NotificationManager.Instance.AddNotification(3f, "Login Successful", Notification.Type.success);
            //if (!CommonData.Instance.fromGame)
           // {
               // GetCredential();
           // }
           // CommonData.Instance.fromGame = false;
            PanelSwitcher.AfterLogin();
        }
    }

    public void ReloadUser()
    {
        if (CommonData.Instance.auth.CurrentUser == null)
        {
            DebugLog("Not signed in, unable to reload user.");
            return;
        }
        DebugLog("Reload User Data");
        CommonData.Instance.auth.CurrentUser.ReloadAsync().ContinueWith(HandleReloadUser);
    }

    void HandleReloadUser(Task authTask)
    {
        if (LogTaskCompletion(authTask, "Reload"))
        {

        }
    }

    public void SignOut()
    {
        DebugLog("Signing out.");
        CommonData.Instance.auth.SignOut();
    }

    public void LogInUser()
    {
        _email = EmailText.text;
        _password = PasswordText.text;
        SigninAsync();
    }

    public void CreateUser()
    {
        _email = EmailText_reg.text;
        _password = PasswordText_reg.text;
        _displayName = Nickname_reg.text;
        CreateUserAsync();
    }

    public void LogInAnon()
    {
        CommonData.Instance.IsSignedIn = false;
        CommonData.Instance.user = "";
        NotificationManager.Instance.AddNotification(2.5f, "Guest Login Successful", Notification.Type.success);
        WelcomeText.SetText("Welcome guest! Would you like to set up an account?");
        PanelSwitcher.AfterLogin();
    }

    public void Logout()
    {
        //CommonData.Instance.fromGame = false;
        NotificationManager.Instance.AddNotification(2.5f, "Logout", Notification.Type.success);
        if (!CommonData.Instance.IsSignedIn)
        {
            SignOut();
        }
        PanelSwitcher.Logout();
    }

    public void ChangePassword()
    {
        string old_pass = PanelSwitcher.ChangePassword.transform.GetChild(0).gameObject.GetComponent<TMP_InputField>().text;
        string new_password = PanelSwitcher.ChangePassword.transform.GetChild(1).gameObject.GetComponent<TMP_InputField>().text;
        string repeatedPassword = PanelSwitcher.ChangePassword.transform.GetChild(2).gameObject.GetComponent<TMP_InputField>().text;

        if (!string.Equals(new_password, repeatedPassword))
        {
            NotificationManager.Instance.AddNotification(3f, "Repeated password doesn't match", Notification.Type.error);
            return;
        }
        if (string.IsNullOrEmpty(old_pass) || string.IsNullOrEmpty(new_password) || string.IsNullOrEmpty(repeatedPassword))
        {
            NotificationManager.Instance.AddNotification(3f, "Fill all fields", Notification.Type.warning);
            return;
        }

        if (CommonData.Instance.IsSignedIn)
        {
            Credential credential = EmailAuthProvider.GetCredential(CommonData.Instance.auth.CurrentUser.Email, old_pass);

            CommonData.Instance.auth.CurrentUser.ReauthenticateAsync(credential).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("ReauthenticateAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    NotificationManager.Instance.AddNotification(3f, "Wrong old password", Notification.Type.error);
                    Debug.LogError("ReauthenticateAsync encountered an error: " + task.Exception);
                    return;
                }
                if (task.IsCompleted)
                {
                    CommonData.Instance.auth.CurrentUser.UpdatePasswordAsync(new_password).ContinueWith(task2 =>
                    {
                        if (task2.IsCanceled)
                        {
                            Debug.LogError("UpdatePasswordAsync was canceled.");
                            NotificationManager.Instance.AddNotification(3f, "Something went wrong", Notification.Type.error);
                            return;
                        }
                        if (task2.IsFaulted)
                        {
                            Debug.LogError("UpdatePasswordAsync encountered an error: " + task2.Exception);
                            return;
                        }
                        NotificationManager.Instance.AddNotification(3f, "Password changed", Notification.Type.success);
                        PanelSwitcher.SwitchScreen(PanelSwitcher.ChangePassword, PanelSwitcher.Settings);
                        Debug.Log("User password updated successfully.");
                    });
                }
            });
        }
    }

    public void DeleteProgress()
    {
        DeleteQuestion.SetActive(true);
    }
    public void DeleteProgressYes()
    {
        DbManager.DeleteUserData(CommonData.Instance.user);
    }
    public void DeleteProgressNo()
    {
        DeleteQuestion.SetActive(false);
    }
}
