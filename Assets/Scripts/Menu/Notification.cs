﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _notificationText;
    [SerializeField]
    private Image _notiType;
    [SerializeField]
    private Sprite _warning;
    [SerializeField]
    private Sprite _error;
    [SerializeField]
    private Sprite _success;

    private string _text = "";
    private float _duration = 3f;
    private bool _isActive = false;
    private CanvasGroup _canvasGroup;

    public enum Type
    {
        none, warning, error, success
    }

    [HideInInspector]
    public Type notificationState = Type.none;

    #region Properties
    public float Duration
    {
        get
        {
            return _duration;
        }

        set
        {
            _duration = value;
        }
    }

    public TextMeshProUGUI NotificationText
    {
        get
        {
            return _notificationText;
        }

        set
        {
            _notificationText = value;
        }
    }

    public Image NotiType
    {
        get
        {
            return _notiType;
        }

        set
        {
            _notiType = value;
        }
    }

    public Sprite Warning
    {
        get
        {
            return _warning;
        }

        set
        {
            _warning = value;
        }
    }

    public Sprite Error
    {
        get
        {
            return _error;
        }

        set
        {
            _error = value;
        }
    }

    public Sprite Success
    {
        get
        {
            return _success;
        }

        set
        {
            _success = value;
        }
    }

    public string Text
    {
        get
        {
            return _text;
        }

        set
        {
            _text = value;
        }
    }

    public bool IsActive
    {
        get
        {
            return _isActive;
        }

        set
        {
            _isActive = value;
        }
    }
    #endregion

    private void Start()
    {
        NotificationText.SetText(Text);
        SetType();
    }
    private void Awake()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        if (IsActive)
        {
            Duration -= Time.deltaTime;
            if (Duration < 1)
            {
                _canvasGroup.alpha = Duration;
            }
            else if (Duration < 0)
            {
                _canvasGroup.alpha = 0f;
            }
            else
            {
                _canvasGroup.alpha = 1f;
            }
            if (Duration < 0)
            {
                NotificationExpired();
            }
        }
    }
    private void NotificationExpired()
    {
        Destroy(gameObject);
    }

    private void SetType()
    {
        switch(notificationState)
        {
            case Type.error:
                NotiType.sprite = Error;
                NotificationText.color = Color.red;
                break;
            case Type.warning:
                NotiType.sprite = Warning;
                NotificationText.color = Color.yellow;
                break;
            case Type.success:
                NotiType.sprite = Success;
                NotificationText.color = new Color32(40, 140, 24, 255);
                break;
            default:
                break;
        }
    }
}



