﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelSwitcher : MonoBehaviour
{

    [SerializeField]
    private GameObject _login;
    [SerializeField]
    private GameObject _main;
    [SerializeField]
    private GameObject _score;
    [SerializeField]
    private GameObject _register;
    [SerializeField]
    private GameObject _settings;
    [SerializeField]
    private GameObject _changePassword;

    #region Properties
    public GameObject Login
    {
        get
        {
            return _login;
        }
    }

    public GameObject Main
    {
        get
        {
            return _main;
        }
    }

    public GameObject Score
    {
        get
        {
            return _score;
        }
    }

    public GameObject Register
    {
        get
        {
            return _register;
        }
    }

    public GameObject Settings
    {
        get
        {
            return _settings;
        }
    }

    public GameObject ChangePassword
    {
        get
        {
            return _changePassword;
        }
    }
    #endregion

    public void CheckScores()
    {
        SwitchScreen(Main, Score);
    }

    public void Logout()
    {
        SwitchScreen(Main, Login);
    }

    public void ScoreToMenu()
    {
        SwitchScreen(Score, Main);
    }
    public void SettingsToMenu()
    {
        SwitchScreen(Settings, Main);
    }
    public void MenuToSettings()
    {
        SwitchScreen(Main, Settings);
    }
    public void RegisterToLogin()
    {
        SwitchScreen(Register, Login);
    }

    public void RegisterButton()
    {
        SwitchScreen(Login, Register);
    }
    public void SettingsToPassword()
    {
        SwitchScreen(Settings, ChangePassword);
    }
    public void PasswordToSettings()
    {
        SwitchScreen(ChangePassword, Settings);
    }

    public void AfterLogin()
    {
        SwitchScreen(Login, Main);
        SwitchScreen(Register, Main);
    }

    public void SwitchScreen(GameObject oldScreen, GameObject newScreen)
    {
        oldScreen.SetActive(false);
        newScreen.SetActive(true);
    }
}
