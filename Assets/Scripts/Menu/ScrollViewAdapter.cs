﻿using Firebase.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewAdapter : MonoBehaviour
{
    #region Fields

    [SerializeField]
    private RectTransform _prefab;
    [SerializeField]
    private ScrollRect _scrollView;
    [SerializeField]
    private RectTransform _content;
    private List<ScoreView> views = new List<ScoreView>();

    #endregion

    #region Private

    private FirebaseDatabase database;
    private int scoresCounter = 9;
    private int lastScore = 0;
    private string lastUID = "";
    private int position = 1;

    #endregion

    #region Properties

    public RectTransform Prefab
    {
        get
        {
            return _prefab;
        }
    }

    public ScrollRect ScrollView
    {
        get
        {
            return _scrollView;
        }
    }

    public RectTransform Content
    {
        get
        {
            return _content;
        }
    }

    protected List<ScoreView> Views
    {
        get
        {
            return views;
        }
    }

    #endregion

    #region MonoBehaviour

    private void OnEnable()
    {
        database = FirebaseDatabase.DefaultInstance;
        ScrollView.verticalScrollbar.onValueChanged.AddListener(ScrollbarCallBack);
        FirstScoresUpdate();
    }

    void OnDisable()
    {
        ScrollView.verticalScrollbar.onValueChanged.RemoveListener(ScrollbarCallBack);
        position = 1;
        Views.Clear();
        lastScore = 0;
        lastUID = "";
        foreach (Transform child in Content.transform)
        {
            Destroy(child.gameObject);
        }
    }

    #endregion

    private void ScrollbarCallBack(float value)
    {
        if (value == 0)
        {
            ScoresUpdate();
        }
    }

    private void FirstScoresUpdate()
    {
        StartCoroutine(FirstFetch(results => OnReceivedData(results)));
    }

    private void ScoresUpdate()
    {
        StartCoroutine(FetchScoresFromFirebase(results => OnReceivedData(results)));
    }

    private void OnReceivedData(List<DataModel> data)
    {
        if (data.Count == 0)
        {
            NotificationManager.Instance.AddNotification(1f, "There's no more scores", Notification.Type.warning);
        }
        else
        {
            for (int i = data.Count - 1; 0 <= i; i--)
            {
                var instance = Instantiate(Prefab.gameObject) as GameObject;
                instance.transform.SetParent(Content, false);
                var view = InitializeScoreView(instance, data[i], position);
                Views.Add(view);
                position++;
            }
            lastScore = Int32.Parse(data[0].score);
        }
    }

    private ScoreView InitializeScoreView(GameObject viewGameObject, DataModel model, int position)
    {
        ScoreView view = new ScoreView(viewGameObject.transform);
        view.nickText.text = model.nick;
        view.positionText.text = position.ToString();
        view.scoreText.text = model.score;
        return view;
    }

    private IEnumerator FirstFetch(Action<List<DataModel>> onDone)
    {
        bool isDone = false;
        List<DataModel> results = new List<DataModel>();
        database.GetReference("leaders")
            .OrderByChild("score")
            .StartAt(lastScore, lastUID)
            .LimitToLast(scoresCounter)
            .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
            {
                if (e2.DatabaseError != null)
                {
                    Debug.LogError(e2.DatabaseError.Message);
                }
                if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
                {
                    foreach (var childSnapshot in e2.Snapshot.Children)
                    {
                        if (childSnapshot.Child("score") == null || childSnapshot.Child("score").Value == null)
                        {
                            NotificationManager.Instance.AddNotification(2.5f, "Error", Notification.Type.error);
                            break;
                        }
                        else
                        {
                            DataModel temp = new DataModel
                            {
                                nick = childSnapshot.Key.ToString(),
                                score = childSnapshot.Child("score").Value.ToString()
                            };
                            results.Add(temp);
                        }
                    }
                    isDone = true;
                }
            };
        yield return new WaitUntil(() => isDone == true);
        yield return SearchForNicknames(results, done => onDone(done));
    }

    private IEnumerator FetchScoresFromFirebase(Action<List<DataModel>> onDone)
    {
        bool isDone = false;
        List<DataModel> results = new List<DataModel>();
        database.GetReference("leaders")
            .OrderByChild("score")
            .EndAt(lastScore, lastUID)
            .LimitToLast(scoresCounter)
            .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
            {
                if (e2.DatabaseError != null)
                {
                    Debug.LogError(e2.DatabaseError.Message);
                }
                if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
                {
                    foreach (var childSnapshot in (e2.Snapshot.Children).Take((int)e2.Snapshot.ChildrenCount - 1))
                    {
                        if (childSnapshot.Child("score") == null || childSnapshot.Child("score").Value == null)
                        {
                            NotificationManager.Instance.AddNotification(2.5f, "Error", Notification.Type.error);
                            break;
                        }
                        else
                        {
                            DataModel temp = new DataModel
                            {
                                nick = childSnapshot.Key.ToString(),
                                score = childSnapshot.Child("score").Value.ToString()
                            };
                            results.Add(temp);
                        }
                    }
                    isDone = true;
                }
            };
        yield return new WaitUntil(() => isDone == true);
        yield return SearchForNicknames(results, done => onDone(done));
    }

    private IEnumerator SearchForNicknames(List<DataModel> scores, Action<List<DataModel>> onDone)
    {
        int scoreCounter = 0;
        if (scores.Count == 0)
        {
            onDone(scores);
        }
        else
        {
            lastUID = scores[0].nick;
            foreach (DataModel score in scores)
            {
                if (score != null)
                {
                    database.GetReference("takenNames/" + score.nick).GetValueAsync().ContinueWith(task =>
                  {
                      if (task.IsFaulted)
                      {
                          Debug.LogError("A error encountered: " + task.Exception);
                      }
                      else if (task.IsCanceled)
                      {
                          Debug.LogError("Canceled ...");
                      }
                      else if (task.IsCompleted)
                      {
                          DataSnapshot snapshot = task.Result;
                          score.nick = snapshot.Child("nickname").Value.ToString();
                          scoreCounter++;
                      }
                  });
                }
            }
        }
        yield return new WaitUntil(() => scoreCounter == scores.Count);
        onDone(scores);
    }

    protected class ScoreView
    {
        public TextMeshProUGUI nickText;
        public TextMeshProUGUI scoreText;
        public TextMeshProUGUI positionText;

        public ScoreView(Transform rootView)
        {
            nickText = rootView.Find("NickImg/Nick").GetComponent<TextMeshProUGUI>();
            scoreText = rootView.Find("ScoreImg/Score").GetComponent<TextMeshProUGUI>();
            positionText = rootView.Find("PosImg/Position").GetComponent<TextMeshProUGUI>();
        }
    }

    protected class DataModel
    {
        public string nick;
        public string score;
    }
}
