﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationManager : Singleton<NotificationManager>
{

    [SerializeField]
    private Notification _prefab;
    [SerializeField]
    private RectTransform _content;
    [SerializeField]
    private Vector3 _position;

    private Notification currentNotification;
    private Queue<Notification> messageQueue = new Queue<Notification>();
    private float uiTextDisplayTimer;

    #region Properties

    public Notification Prefab
    {
        get
        {
            return _prefab;
        }
    }

    public RectTransform Content
    {
        get
        {
            return _content;
        }
        set
        {
            _content = value;
        }
    }

    public Vector3 Position
    {
        get
        {
            return _position;
        }
    }

    #endregion

    private Notification instantiateNotification(float duration, string text, Notification.Type type)
    {
        Notification instance = Instantiate(Prefab, Position, Quaternion.identity);
        instance.transform.SetParent(Content, false);
        instance.Duration = duration;
        instance.notificationState = type;
        instance.Text = text;
        instance.gameObject.SetActive(false);
        return instance;
    }

    public void AddNotification(float duration, string text, Notification.Type type)
    {
        Notification notification = instantiateNotification(duration, text, type);
        messageQueue.Enqueue(notification);
    }

    void Update()
    {
        if (messageQueue.Count > 0)
        {
            if (currentNotification == null)
            {
                currentNotification = messageQueue.Dequeue();
                currentNotification.gameObject.SetActive(true);
                currentNotification.IsActive = true;
            }
        }
    }
}
