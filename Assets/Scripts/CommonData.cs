﻿using Firebase.Auth;
using UnityEngine;

public class CommonData : MonoBehaviour
{
    public static CommonData Instance;
    private bool isSignedIn = false;
    public string user;
    public Credential cred;
    public FirebaseAuth auth;

    public bool IsSignedIn
    {
        get
        {
            if (this != null)
            {
                return isSignedIn;
            }
            else return false;
        }

        set
        {
            isSignedIn = value;
        }
    }

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(this);
    }
}

