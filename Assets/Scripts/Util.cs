﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util : Singleton<Util>
{

    public LayerMask GROUND;
    public LayerMask PLAYER;
    public LayerMask ENEMY;
    public LayerMask RETURN_TO_POOL;

    public bool IsInLayerMask(int layer, LayerMask layermask)
    {
        return layermask == (layermask | (1 << layer));
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
}
