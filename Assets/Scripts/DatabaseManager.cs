﻿using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseManager : MonoBehaviour
{
    private FirebaseDatabase database;

    void Start()
    {
        InitializeFirebase();
    }

    private void InitializeFirebase()
    {
        database = FirebaseDatabase.DefaultInstance;
        FirebaseApp app = FirebaseApp.DefaultInstance;
        app.SetEditorDatabaseUrl("https://engineeringthesis-pp.firebaseio.com");
        if (app.Options.DatabaseUrl != null) app.SetEditorDatabaseUrl(app.Options.DatabaseUrl);
    }

    private void DebugLog(string s)
    {
        Debug.Log(s);
    }

    public void AddScore()
    {
        AddBestScore();
        GeneralScore();    
    }

    private void AddBestScore()
    {
        DatabaseReference reference = database.GetReference(CommonData.Instance.user + "/best");
        reference.RunTransaction(AddUserHighestScore)
          .ContinueWith(task =>
          {
              if (task.Exception != null)
              {
                  DebugLog(task.Exception.ToString());
              }
              else if (task.IsCompleted)
              {
                  DebugLog("Best Transaction complete.");
              }
          });
    }

    private void GeneralScore()
    {
        DatabaseReference reference = database.GetReference(CommonData.Instance.user + "/general");
        reference.RunTransaction(AddGeneralScore)
          .ContinueWith(task =>
          {
              if (task.Exception != null)
              {
                  DebugLog(task.Exception.ToString());
              }
              else if (task.IsCompleted)
              {
                  DebugLog("General Transaction complete.");
              }
          });
    }

    private TransactionResult AddUserHighestScore(MutableData currentData)
    {
        Dictionary<string, object> score = currentData.Value as Dictionary<string, object>;
        Dictionary<string, object> entryValues = GameData.Instance.ToDictionary();
        Dictionary<string, object> finalValues = new Dictionary<string, object>();

        if (score == null)
        {
            currentData.Value = entryValues;
            return TransactionResult.Success(currentData);
        }
        else
        {
            foreach (string key in entryValues.Keys)
            {
                Int64 score1 = (Int64)score[key];
                int score2 = (int)entryValues[key];
                if (score2 > score1)
                {
                    if (key == "score")
                    {
                        PushHighestScore(score2);
                    }
                    finalValues.Add(key, score2);
                }
                else
                {
                    finalValues.Add(key, score1);
                }
            }
        }
        currentData.Value = finalValues;
        return TransactionResult.Success(currentData);
    }

    private TransactionResult AddGeneralScore(MutableData currentData)
    {
        Dictionary<string, object> score = currentData.Value as Dictionary<string, object>;
        Dictionary<string, object> entryValues = GameData.Instance.ToDictionary();
        Dictionary<string, object> finalValues = new Dictionary<string, object>();
        if (score == null)
        {
            entryValues.Remove("level");
            entryValues.Remove("score");
            currentData.Value = entryValues;
            return TransactionResult.Success(currentData);
        }
        else
        {
            foreach (string key in score.Keys)
            {
                Int64 score1 = (Int64)score[key];
                int score2 = (int)entryValues[key];
                finalValues.Add(key, score1 + score2);
            }
        }
        currentData.Value = finalValues;
        return TransactionResult.Success(currentData);
    }

    public void PushHighestScore(Int64 score)
    {
        DatabaseReference reference = database.RootReference.Child("leaders");
        Dictionary<string, object> data = new Dictionary<string, object>();
        Dictionary<string, object> dscore = new Dictionary<string, object>();
        data.Add("score", score);
        dscore.Add(CommonData.Instance.user, data);
        reference.UpdateChildrenAsync(dscore);
    }

    public void PushUser(string uid, string email, string name)
    {
        DatabaseReference reference = database.RootReference.Child(uid);
        DatabaseReference reference_names = database.RootReference.Child("takenNames");
        Dictionary<string, object> data = new Dictionary<string, object>();
        data.Add("email", email);
        data.Add("nickname", name);
        reference.UpdateChildrenAsync(data);
        Dictionary<string, object> data2 = new Dictionary<string, object>();
        Dictionary<string, object> dscore2 = new Dictionary<string, object>();
        data2.Add("nickname", name);
        dscore2.Add(uid, data2);
        reference_names.UpdateChildrenAsync(dscore2);
    }

    public void DeleteUserData(string uid)
    {
        DatabaseReference best = database.RootReference.Child(uid + "/best");
        DatabaseReference general = database.RootReference.Child(uid + "/general");
        DatabaseReference leaders = database.RootReference.Child("leaders");
        best.RemoveValueAsync();
        general.RemoveValueAsync();
        Dictionary<string, object> data2 = new Dictionary<string, object>();
        data2.Add(uid, null);
        leaders.UpdateChildrenAsync(data2);
    }

    public IEnumerator IsNickTaken(String nick, Action<bool> onDone)
    {
        bool isDone = false;
        bool nickIsTaken = false;
        database.RootReference
           .OrderByChild("nickname").EqualTo(nick)
           .ValueChanged += (object sender2, ValueChangedEventArgs e2) =>
           {
               if (e2.DatabaseError != null)
               {
                   Debug.LogError(e2.DatabaseError.Message);
               }
               Debug.Log("Received values for Nick.");
               if (e2.Snapshot != null && e2.Snapshot.ChildrenCount > 0)
               {
                   nickIsTaken = true;
               }
               isDone = true;
           };
        yield return new WaitUntil(() => isDone == true);
        onDone(nickIsTaken);
    }

    public IEnumerator GetNick(Action<string> onDone)
    {
        bool isDone = false;
        string nick = "";
        database.RootReference
           .Child(CommonData.Instance.user + "/nickname").GetValueAsync().ContinueWith(task =>
           {
               if (task.Exception != null)
               {
                   DebugLog(task.Exception.ToString());
                   onDone("");
               }
               else if (task.IsCompleted)
               {
                   DebugLog("Best Transaction complete.");
                   DataSnapshot snapshot = task.Result;
                   nick = snapshot.Value.ToString();
                   isDone = true;
               }
           });
        yield return new WaitUntil(() => isDone == true);
        onDone(nick);
    }
}
