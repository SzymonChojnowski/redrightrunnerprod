# Red Right Runner

Platformowa gra 2D - Infinite Runner w którym unika się przeciwników, zbiera monety i ulepszenia. Wyniki wysyłane są 
na serwer Google Firebase Database.

Filmik przedstawia starszą wersje gry. Problemy z animacją zostały już naprawione.
[![Filmik z gry](https://img.youtube.com/vi/f7wesne_gaY/0.jpg)](https://www.youtube.com/watch?v=f7wesne_gaY)


# Readme

Kod jest aktualnie poddawany refaktoryzacji.
Zająłem się przepisaniem implementacji gracza i gry z użyciem Wzorca Stanu.
Następnie mam zamiar pozbyć się menadżerów - rozbić je na mniejsze klasy i funkjonalności.


### Instalacja

Gra została stworzona z wykorzystaniem Unity 2018.2.11f1
Wykorzystuje TextMeshPro oraz [Firebase SDK][df1]
Do odpowiedniego działania po sklonowaniu potrzebny jest plik googleservices.json,
który można wygenerować na własnym koncie Google Firebase.

### Funckjonalności i zawartość

 - Logowanie / Rejestracja
 - Tryb Gościa
 - Podgląd wyników w grze
 - Usuwanie wyników z serwera przez gracza
 - Zmiana hasła
 - Generowanie losowych poziomów
 - Przeciwnicy: Jumper i SinusFly
 - 5 Power-upów: Nieśmiertelność, Magnes, Coin Multiplayer, Spawner skrzynek, Losowy bonus
 - Możliwość biegu i skoku
 - Zabijanie przeciwników + wykrywanie "Double Kill"
 - Sparametryzowany generator mapy (Możliwość dostosowania rozmiaru platform, częstości wystąpień etc...)
 - Zbieranie rozmaitych statystyk podczas gry i wysyłanie ich na serwer
 - Object Pooling, State Pattern, Eventy, Singletony

### To-Do

 - Reklamy - AdMob
 - Dodanie particles
 - Zmiana logowania e-mail/hasło na Google Play Authentication
 - Dostosowanie parametrów mapy w zależności od poziomu
 - Zmiana "Active Powerups" z formy tekstowej na obrazki z licznikiem czasu
 - Nowe rodzaje przeciwników / ulepszeń
 - Osiągnięcia
 - Refaktor


   [df1]: <https://firebase.google.com/docs/unity/setup>
